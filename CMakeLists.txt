###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Choose CMake policies:
CMAKE_POLICY(SET CMP0048 NEW)   # Project command sets version variable.
CMAKE_POLICY(SET CMP0063 NEW)   # Project command sets version variable.

# Require a modern version of CMake.
CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0 FATAL_ERROR)

# Define project name and version number.
PROJECT(atomicrex VERSION 1.0.3 LANGUAGES CXX C Fortran)

# Set default build type to "Release"
IF(NOT CMAKE_BUILD_TYPE) 
    SET(CMAKE_BUILD_TYPE Release)
ENDIF()

# Look in cmake subdirectory when searching for CMake modules.
LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# Check for compiler OpenMP support.
FIND_PACKAGE(OpenMP)

OPTION(USE_OPENMP "Use OpenMP parallelization" ${OPENMP_FOUND})
OPTION(ENABLE_NLOPT "Enable NLopt minimizer component" "ON")
OPTION(ENABLE_PYTHON_INTERFACE "Build Python interface to atomicrex" "ON")

# Some C++ compilers don't ship with the new C++ filesystem TS library yet.
# We fall back to the Boost.Filesystem library by default.
SET(USE_BOOST_FILESYSTEM_LIB_BY_DEFAULT ON)
OPTION(USE_BOOST_FILESYSTEM_LIB "Use Boost.Filesystem library for backward compatibility instead of C++ filesystem TS, which is only supported by recent compilers" "${USE_BOOST_FILESYSTEM_LIB_BY_DEFAULT}")

# Define code version string.
INCLUDE(cmake/Version.cmake)
# Set up flags for Fortran compiler.
INCLUDE(cmake/FortranCompilerFlags.cmake)

# Enable C++11 standard.
IF(NOT CMAKE_VERSION VERSION_LESS "3.1")
	SET(CMAKE_CXX_STANDARD 11)
	SET(CMAKE_CXX_STANDARD_REQUIRED ON)
ELSE()
   	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
ENDIF()

# Find Boost library.
IF(NOT USE_BOOST_FILESYSTEM_LIB)
	FIND_PACKAGE(Boost REQUIRED)
ELSE()
	FIND_PACKAGE(Boost REQUIRED COMPONENTS filesystem)
ENDIF()
IF(NOT Boost_FOUND)
	MESSAGE(FATAL_ERROR "Boost library not found. Reason: ${Boost_ERROR_REASON}")
ENDIF()

# Find libxml2 library.
FIND_PACKAGE(LibXml2 REQUIRED)
IF(NOT LIBXML2_FOUND)
	MESSAGE(FATAL_ERROR "libxml2 library not found.")
ENDIF()

# Find NLopt library.
IF(ENABLE_NLOPT)
    FIND_PACKAGE(Nlopt REQUIRED)
    IF(NOT NLOPT_FOUND)
	    MESSAGE(FATAL_ERROR "NLopt not found. Please install (http://ab-initio.mit.edu/wiki/index.php/NLopt) and specify its location by setting NLOPT_INCLUDE_DIR and NLOPT_LIBRARY. Or set ENABLE_NLOPT to OFF in CMake settings to disable the NLopt component completely.")
	ENDIF()
ENDIF()

# Compile code.
ADD_SUBDIRECTORY(src)

# Build user documentation
INCLUDE(cmake/Documentation.cmake)

# Generate automated tests
INCLUDE(cmake/Testing.cmake)
