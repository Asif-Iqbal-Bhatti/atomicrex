###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Find muParser library.
LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")
FIND_PACKAGE(MuParser QUIET)

IF(MUPARSER_FOUND)

	# Create imported target for the existing muParser library.
	MESSAGE(STATUS "Using external muParser library:")
	MESSAGE(STATUS "  lib: ${MuParser_LIBRARIES}")
	MESSAGE(STATUS "  headers: ${MuParser_INCLUDE_DIRS}")
	ADD_LIBRARY(muParser SHARED IMPORTED GLOBAL)
	SET_PROPERTY(TARGET muParser PROPERTY IMPORTED_LOCATION "${MuParser_LIBRARIES}")
	SET_PROPERTY(TARGET muParser APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${MuParser_INCLUDE_DIRS}")
	
ELSE()

	MESSAGE(STATUS "Using internal copy of muParser library")

	# Build our own version of the library.
	SET(MUPARSER_SOURCE_FILES
		muParser.cpp
		muParserBytecode.cpp
		muParserInt.cpp
		muParserTokenReader.cpp
		muParserError.cpp
		muParserCallback.cpp
		muParserBase.cpp
	)

	# Honor visibility properties for all target types.
	# This is needed so we can set the symbol visibility of the compile target to hidden below. 
	CMAKE_POLICY(SET CMP0063 NEW)

	# Build library.
	ADD_LIBRARY(muParser STATIC ${MUPARSER_SOURCE_FILES})

	# Make header files of this library available to dependent targets.
	TARGET_INCLUDE_DIRECTORIES(muParser INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}")

    # Since we will link this into the Particles plugin, we need to use the same setting for the fPIC flag.
	SET_PROPERTY(TARGET muParser PROPERTY POSITION_INDEPENDENT_CODE ON)
		
	# Compile with symbols hidden by default. This is needed for linking with pybind11 later. 
	SET_TARGET_PROPERTIES(muParser PROPERTIES CXX_VISIBILITY_PRESET "hidden")

ENDIF()
