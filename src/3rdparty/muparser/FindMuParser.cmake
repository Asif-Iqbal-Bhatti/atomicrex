###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Try to find the muParser library
#  MUPARSER_FOUND - system has muParser lib
#  MuParser_INCLUDE_DIRS - the include directories needed
#  MuParser_LIBRARIES - libraries needed

FIND_PATH(MUPARSER_INCLUDE_DIR NAMES muParser.h)
FIND_LIBRARY(MUPARSER_LIBRARY NAMES muparser)

SET(MuParser_INCLUDE_DIRS ${MUPARSER_INCLUDE_DIR})
SET(MuParser_LIBRARIES ${MUPARSER_LIBRARY})

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MuParser DEFAULT_MSG MUPARSER_LIBRARY MUPARSER_INCLUDE_DIR)

MARK_AS_ADVANCED(MUPARSER_INCLUDE_DIR MUPARSER_LIBRARY)
