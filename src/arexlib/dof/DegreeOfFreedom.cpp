///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DegreeOfFreedom.h"
#include "../structures/AtomicStructure.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Assigns a new value to the DOF.
******************************************************************************/
ScalarDOF& ScalarDOF::operator=(double value)
{
    _value = value;
    if(object()) object()->dofValueChanged(*this);
    for(auto& f : _changeListeners) f(*this);
    return *this;
}

/******************************************************************************
* Lets the DOF export its current value(s) into the given vector.
******************************************************************************/
void ScalarDOF::exportValue(double*& dst) { *dst++ = _value; }

/******************************************************************************
* Lets the DOF import its value(s) from the given vector.
******************************************************************************/
void ScalarDOF::importValue(const double*& src)
{
    _value = *src++;
    if(object()) object()->dofValueChanged(*this);
    for(auto& f : _changeListeners) f(*this);
}

/******************************************************************************
* Outputs the current value of the DOF.
******************************************************************************/
void ScalarDOF::print(MsgLogger& stream)
{
    if(object()) {
        object()->print(stream);
        stream << ".";
    }
    stream << id();
    if(tag().empty() == false) stream << "[" << tag() << "]";
    stream << ": " << _value;

    if(hasLowerBound() && hasUpperBound())
        stream << " [" << lowerBound() << ":" << upperBound() << "]";
    else if(hasLowerBound())
        stream << " [" << lowerBound() << ":]";
    else if(hasUpperBound())
        stream << " [:" << upperBound() << "]";

    if(_masterDOF) {
        stream << " = ";
        _masterDOF->print(stream);
    }
}

/******************************************************************************
* Sets this DOF to be equal to another DOF.
* This DOF will no longer be a real DOF.
* Every time the master DOF changes, the value of this DOF will be updated accordingly.
******************************************************************************/
bool ScalarDOF::setEqualTo(ScalarDOF* masterDOF)
{
    if(masterDOF) {
        // Avoid the possibility of infinite recursion.
        if(masterDOF == this || masterDOF->_masterDOF != nullptr) return false;

        _masterDOF = masterDOF;

        setFitEnabled(false);
        setRelax(false);
        setInitialValue(masterDOF->initialValue());

        // Listen for changes of master DOF, so we can update our own value accordingly.
        _masterDOF->_changeListeners.push_back([this](ScalarDOF& changingDOF) {
            if(_masterDOF == &changingDOF) *this = (double)changingDOF;
        });

        return true;
    }
    else {
        _masterDOF = nullptr;
        return false;
    }
}

/******************************************************************************
* Parse the contents of the <fit> element in the job file.
******************************************************************************/
void DegreeOfFreedom::parseFit(XML::Element fitElement)
{
    if(fitElement.parseOptionalBooleanParameterAttribute("enabled", true)) setFitEnabled(true);

    if(!fitElement.parseOptionalBooleanParameterAttribute("reset", true)) setResetBeforeRelax(false);
}

/******************************************************************************
* Parse the contents of the <relax> element in the job file.
******************************************************************************/
void DegreeOfFreedom::parseRelax(XML::Element relaxElement)
{
    if(relaxElement.parseOptionalBooleanParameterAttribute("relax", true)) setRelax(true);
    if(!relaxElement.parseOptionalBooleanParameterAttribute("reset", true)) setResetBeforeRelax(false);
}

/******************************************************************************
* Produces an XML representation of the DOF's current value.
******************************************************************************/
XML::OElement DegreeOfFreedom::generateXMLValueDefinition() const { return XML::OElement(id().c_str()); }

/******************************************************************************
* Produces an XML representation of the DOF's fit settings.
******************************************************************************/
XML::OElement DegreeOfFreedom::generateXMLFitDefinition() const
{
    XML::OElement element(id().c_str());
    element.setAttribute("enabled", fitEnabled() ? "true" : "false");
    if(resetBeforeRelax()) element.setAttribute("reset", "true");
    return element;
}

/******************************************************************************
* Parse the contents of the <fit> element in the job file.
******************************************************************************/
void ScalarDOF::parseFit(XML::Element fitElement)
{
    DegreeOfFreedom::parseFit(fitElement);

    if(fitElement.hasAttribute("min")) setLowerBound(fitElement.parseFloatParameterAttribute("min"));
    if(fitElement.hasAttribute("max")) setUpperBound(fitElement.parseFloatParameterAttribute("max"));

    if(fitElement.hasAttribute("equalto")) {
        parseSetEqualToAttribute(fitElement);
    }
}

/******************************************************************************
* Parses the 'equalto' attribute and links the DOF to another master DOF.
******************************************************************************/
void ScalarDOF::parseSetEqualToAttribute(XML::Element element)
{
    // Parse ID and tag of the DOF this DOF should be set equal to.
    FPString dofId = element.parseStringParameterAttribute("equalto");

    // Extract object.
    auto pos = dofId.find('.');
    FitObject* obj = object();
    // If the dofId contains a "." it belongs to a structure. Then check if the referenced
    // structure exists and add it to the list of referenced structures.
    if(pos != FPString::npos) {
        FPString objectId = dofId.substr(0, pos);
        dofId = dofId.substr(pos + 1);

        // Look up referenced structure.
        obj = object()->job()->getAtomicStructure(objectId);
        if(!obj)
            throw runtime_error(str(format("Invalid reference in line %1% of XML file: Non-existing structure \"%2%\".") %
                                    element.lineNumber() % objectId));
        // Add structure to the list of referenced structures.
        object()->job()->addReferencedStructure(object()->job()->getAtomicStructure(objectId));
    }

    // Extract tag.
    FPString dofTag;
    pos = dofId.find('[');
    if(pos != FPString::npos) {
        dofTag = dofId.substr(pos + 1, dofId.size() - pos - 2);
        dofId.resize(pos);
    }

    // Look up the DOF.
    ScalarDOF* masterDof = dynamic_cast<ScalarDOF*>(obj->DOFById(dofId, dofTag));
    if(masterDof == nullptr)
        throw runtime_error(
            str(format("Invalid reference in line %1% of XML file: Non-existing degree of freedom \"%2%\" (tag=%3%).") %
                element.lineNumber() % dofId % (dofTag.empty() ? FPString("<empty>") : dofTag)));

    if(!setEqualTo(masterDof))
        throw runtime_error(str(format("Invalid reference in line %1% of XML file: Cannot slave degree of freedom to another DOF "
                                       "that is slaved itself.") %
                                element.lineNumber()));
}

/******************************************************************************
* Parse the contents of the <relax> element in the job file.
******************************************************************************/
void ScalarDOF::parseRelax(XML::Element relaxElement)
{
    DegreeOfFreedom::parseRelax(relaxElement);

    if(relaxElement.hasAttribute("min")) setLowerBound(relaxElement.parseFloatParameterAttribute("min"));
    if(relaxElement.hasAttribute("max")) setUpperBound(relaxElement.parseFloatParameterAttribute("max"));
}

/******************************************************************************
* Produces an XML representation of the DOF's current value.
******************************************************************************/
XML::OElement ScalarDOF::generateXMLValueDefinition() const
{
    XML::OElement dofElement = DegreeOfFreedom::generateXMLValueDefinition();
    dofElement.setTextContent(std::to_string(_value));
    return dofElement;
}

/******************************************************************************
* Produces an XML representation of the DOF's fit settings.
******************************************************************************/
XML::OElement ScalarDOF::generateXMLFitDefinition() const
{
    XML::OElement element = DegreeOfFreedom::generateXMLFitDefinition();

    if(hasLowerBound()) element.setAttribute("min", std::to_string(lowerBound()));
    if(hasUpperBound()) element.setAttribute("max", std::to_string(upperBound()));

    return element;
}
}
