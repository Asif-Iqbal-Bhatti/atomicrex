///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DegreeOfFreedom.h"

namespace atomicrex {

class AtomicStructure;

/**
* @brief      This DOF manages the atomic positions in a structure.
*
* @details    It allows to relax these positions to minimize the energy of the structure.
*             Note that this DOF does not store the atomic coordinates. It only provides a
*             facade for the atomic positions stored by the AtomicStructure class.
*/
class AtomCoordinatesDOF : public DegreeOfFreedom
{
public:
    /// Constructor.
    AtomCoordinatesDOF() : DegreeOfFreedom("atom-coordinates") {}

    /// Returns the number of scalar degrees of freedom this DOF is composed of.
    virtual int numScalars() override;

    /// Lets the DOF export its current value(s) into the given value array.
    virtual void exportValue(double*& dst) override;

    /// Python interface compatibility only for now.
    virtual void exportUpperBound(double*& dst) override {};

    /// Python interface compatibility only for now.
    virtual void exportLowerBound(double*& dst) override {};

    /// Lets the DOF import its value(s) from the given value array.
    virtual void importValue(const double*& src) override;

    /// Returns the structure to which this DOF belongs.
    AtomicStructure* structure() const;

    /// Resets the value of the DOF to what was given in the job file.
    virtual void reset() override;
};

}  // End of namespace
