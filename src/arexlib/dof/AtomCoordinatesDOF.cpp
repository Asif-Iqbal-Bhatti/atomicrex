///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DegreeOfFreedom.h"
#include "../structures/AtomicStructure.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Returns the structure to which this DOF belongs.
******************************************************************************/
AtomicStructure* AtomCoordinatesDOF::structure() const { return static_cast<AtomicStructure*>(object()); }

/******************************************************************************
* Reset the atom positions to the values supplied in the job file.
******************************************************************************/
void AtomCoordinatesDOF::reset()
{
    DegreeOfFreedom::reset();
    structure()->setAtomPositions(structure()->initialAtomPositions());
}

/******************************************************************************
* Returns the number of scalar degrees of freedom this DOF is composed of.
******************************************************************************/
int AtomCoordinatesDOF::numScalars() { return std::max((structure()->numLocalAtoms() - 1) * 3, 0); }

/******************************************************************************
* Lets the DOF export its current value(s) into the given vector.
******************************************************************************/
void AtomCoordinatesDOF::exportValue(double*& dst)
{
    memcpy(dst, &structure()->atomPositions().front(), sizeof(double) * numScalars());
    dst += numScalars();
}

/******************************************************************************
* Lets the DOF import its value(s) from the given vector.
******************************************************************************/
void AtomCoordinatesDOF::importValue(const double*& src)
{
    int n = numScalars();
    if(n == 0) return;

    memcpy(&structure()->atomPositions().front(), src, sizeof(double) * n);
    src += n;

    structure()->setDirty(AtomicStructure::ATOM_POSITIONS);
}
}
