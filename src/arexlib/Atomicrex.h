///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

/******************************************************************************
* STL Library
******************************************************************************/
#include <vector>
#include <list>
#include <map>
#include <set>
#include <limits>
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <locale>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <string>
#include <cctype>

/******************************************************************************
* C++ filesystem library
******************************************************************************/
#ifndef USE_BOOST_FILESYSTEM_LIB
#include <experimental/filesystem>
#else
#include <boost/filesystem.hpp>
#endif

/******************************************************************************
* Boost library
******************************************************************************/
#include <boost/utility.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/algorithm_ext/insert.hpp>
#include <boost/format.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/atomic.hpp>

namespace atomicrex {

/// The default string type used throughout the code:
using FPString = std::string;

}  // End of namespace

/******************************************************************************
* Include a standard set of our own headers
******************************************************************************/
#include "util/FloatType.h"
#include "util/Logger.h"
#include "util/Filesystem.h"
#include "util/linalg/LinAlg.h"
