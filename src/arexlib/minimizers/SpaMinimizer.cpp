///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "SpaMinimizer.h"
#include "../job/FitJob.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor that copies another minimizer
******************************************************************************/
SpaMinimizer::SpaMinimizer(const SpaMinimizer& other) : Minimizer(other)
{
    _submin = other._submin->clone();
}

/******************************************************************************
* Initializes the minimizer.
******************************************************************************/
void SpaMinimizer::prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                           const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient)
{
    _bestParameters.resize(job()->numActiveDOF());
    _bestResidual = numeric_limits<double>::infinity();

    Minimizer::prepare(std::move(x0), func, gradient);

    BOOST_ASSERT_MSG(_submin, "Spa needs a secondary minimizer.");
    _submin->prepare(std::move(x0), func, gradient);
}

/******************************************************************************
* Sets constraints for the variation of the parameters.
******************************************************************************/
void SpaMinimizer::setConstraints(std::vector<BoundConstraints>&& constraintTypes, std::vector<double>&& lowerBounds,
                                  std::vector<double>&& upperBounds)
{
    _constraintTypes = std::move(constraintTypes);
    _lowerBounds = std::move(lowerBounds);
    _upperBounds = std::move(upperBounds);

    BOOST_ASSERT(_submin);
    _submin->setConstraints(std::vector<BoundConstraints>(_constraintTypes), std::vector<double>(_lowerBounds),
                            std::vector<double>(_upperBounds));
}

/******************************************************************************
* Parses the minimizer's parameters from the XML file.
******************************************************************************/
void SpaMinimizer::parse(XML::Element minimizerElement)
{
    Minimizer::parse(minimizerElement);

    XML::Element sub = minimizerElement.firstChildElement();
    if(!sub)
        throw runtime_error(str(format("<%1%> element in line %2% of XML file must contain a sub-minimizer.") %
                                minimizerElement.tag() % minimizerElement.lineNumber()));

    _submin = createAndParse(sub, job());

    // Parse the seed and initialize the random number generator.
    FPString s = minimizerElement.parseStringParameterAttribute("seed");
    std::seed_seq seed(s.begin(), s.end());
    _gen.seed(seed);
}

/******************************************************************************
* Do one iteration of Spa. Internally uses BFGS. Returns SpaResult.
******************************************************************************/
Minimizer::MinimizerResult SpaMinimizer::iterate()
{
    _itercount++;

    std::vector<double> p(job()->numActiveDOF());
    p = randomParameters();

    // Define the objective function.
    auto function = [this](const vector<double>& k) -> double {
        // Unpack values from the linear vector and store them in the DOF objects.
        job()->unpackDOF(k);
        // Perform the actual calculation of the residual, i.e. deviation from the target property values.
        return job()->calculateResidual();
    };

    _submin->prepare(std::move(p), function);

    // Minimization loop.
    MinimizerResult result;
    while((result = _submin->iterate()) == MINIMIZATION_CONTINUE &&
          _submin->itercount() <= _submin->maximumNumberOfIterations()) {
        MsgLogger(debug) << "  iter= " << _submin->itercount() << " residual= " << _submin->value()
                         << " grad= " << _submin->gradientNorm2() << endl;
        // Check if stop file has been created.
        ifstream stopFile("stop_fitpot");
        if(stopFile.is_open()) {
            MsgLogger(minimum) << "Found 'stop_fitpot' file. Minimization will be terminated." << endl;
            break;
        }
    }

    if(result == MINIMIZATION_ERROR)
        throw runtime_error("The minimizer failed with an error while minimizing the objective function.");

    // Get residual of last parameter set.
    f = _submin->value();

    // Save new Records.
    if(f < _bestResidual) {
        MsgLogger(medium) << "Found new record! Residual = " << f << endl;
        _bestResidual = f;
        job()->packDOF(_bestParameters);

        // Write current state to file
        for(Potential* pot : job()->potentials()) pot->outputResults();
    }

    // Output current step
    printStep();

    // At the last iteration: select the best parameters found so far.
    if(_itercount > _maximumNumberOfIterations) {
        MsgLogger(medium) << "Reset Parameters! Residual: " << _bestResidual << endl;
        job()->unpackDOF(_bestParameters);
    }

    return MINIMIZATION_CONTINUE;
}

/******************************************************************************
* Create a vector of random parameters.
******************************************************************************/
std::vector<double> SpaMinimizer::randomParameters()
{
    std::vector<double> p;

    for(int i = 0; i < job()->numActiveDOF(); i++) {
        double newValue = _lowerBounds[i] + _dis(_gen) * (_upperBounds[i] - _lowerBounds[i]);
        p.push_back(newValue);
    }
    return p;
}

/******************************************************************************
* Function that creates a copy of this minimizer.
******************************************************************************/
std::unique_ptr<Minimizer> SpaMinimizer::clone()
{
    return std::unique_ptr<SpaMinimizer>(new SpaMinimizer(*this));
}

}  // End of namespace.
