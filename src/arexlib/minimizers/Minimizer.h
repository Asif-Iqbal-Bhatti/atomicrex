///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

class FitJob;  // defined in FitJob.h

/**
  @brief Abstract base class for minimization algorithms.
 */
class Minimizer
{
public:
    // This enumeration indicates the result of a minimization step.
    enum MinimizerResult
    {
        // The minimization continues.
        MINIMIZATION_CONTINUE,
        // The minimization has converged.
        MINIMIZATION_CONVERGED,
        // The minimization step has returned an abnormal result.
        MINIMIZATION_ABNORMAL,
        // An error was encountered during minimization.
        MINIMIZATION_ERROR,
    };

    // This enumeration indicates the treatment of bounds on parameters during the minimization
    enum BoundConstraints
    {
        // No bounds on parameters.
        NO_BOUNDS = 0,
        // Lower bounds are enforced.
        LOWER_BOUNDS = 1,
        // Both lower and upper bounds are enforced.
        BOTH_BOUNDS = 2,
        // Upper bounds are enforced.
        UPPER_BOUNDS = 3
    };

public:
    /// Constructor.
    ///
    /// \param job Pointer to the Fitjob
    /// \param name The name of the minimizer type.
    /// \param intervalForPrinting Determines the frequency for printing the DOF list.
    /// \param isFitMinimizer Flag that saves if the minimizer is used for structure optimization or
    ///                       parameter optimization.
    Minimizer(FitJob* job, int intervalForPrinting, FPString name, bool isFitMinimizer)
        : _job(job), _intervalForPrinting(intervalForPrinting), _name(name), _isFitMinimizer(isFitMinimizer)
    {
    }

    /// Constructor that copies another minimizer.
    ///
    /// \param other LBFGS minimizer that is copied.
    Minimizer(const Minimizer& other);

    /// Destructor
    virtual ~Minimizer() = default;

    /// Returns the job to which this object belongs.
    FitJob* job() const { return _job; }

    /**
       @brief Initializes the minimizer by setting the objective function and the starting vector.
       @details Must be called once before entering the minimization loop.

       @param x0 An R-value reference to the starting vector.
       The length of this vector determines the number of dimensions.
       The function transfers the vector to internal storage. That means the
       passed-in vector will no longer be valid after the function returns.
       @param func The object that computes the value of the objective function at a given point x.
       @param gradient An optional function object that computes the (analytic) gradient of the objective function
       at a given point x (and also the value of the objective function).
       If no gradient function is provided, and the minimization algorithm requires
       the gradient, the minimizer will compute it using finite differences by
       evaluating \a func several times.
    */
    virtual void prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                         const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient =
                             std::function<double(const std::vector<double>&, std::vector<double>&)>());

    /**
       @brief Sets constraints for variations of the parameters.
       @details Must be called after prepare() and before the minimization loop is entered.
       All arguments passed to this function will be transferred into internal storage of this class.
       The input vectors become invalid when the function returns.
    */
    virtual void setConstraints(std::vector<BoundConstraints>&& constraintTypes, std::vector<double>&& lowerBounds,
                                std::vector<double>&& upperBounds)
    {
    }

    /// Performs one minimization iteration.
    virtual MinimizerResult iterate() = 0;

    /// Returns the number of iterations performed so far.
    int itercount() const { return _itercount; }

    /// Return the best residual found so far.
    double bestResidual() const { return _bestResidual; }
    
    /// Returns the best parameter set
    const std::vector<double>& bestParameters() const { return _bestParameters; }

    /// Returns the lower and upper bounds
    const std::vector<double>& lowerBounds() const { return _lowerBounds; }
    const std::vector<double>& upperBounds() const { return _upperBounds; }

    /// Returns the value of the objective function at the current x.
    double value() const { return f; }

    /// Returns the 2-norm of the gradient of the objective function at the current x.
    double gradientNorm2() const { return _gradNorm2; }

    /// Returns the limit for which the minimizer converges.
    double convergenceThreshold() const { return _convergenceThreshold; }

    /// Returns the maximum number of iterations for the minimizer.
    int maximumNumberOfIterations() const { return _maximumNumberOfIterations; }

    /// Set the maximum number of iterations for the minimizer.
    void setMaximumNumberOfIterations(int num) { _maximumNumberOfIterations = num; }

    /// Print the current status to command line
    void printStep() { printStep(f); }

    /// Print the current status to command line
    ///
    /// \param residual Residual that gets printed.
    void printStep(double residual);

    /// Parses the minimizer's parameters from the XML file.
    virtual void parse(XML::Element minimizerElement);

    /// Factory function that creates an instance of the minimizer type that is defined in the XML file.
    ///
    /// \param job Pointer to the Fitjob
    /// \param intervalForPrinting Determines the frequency for printing the DOF list.
    /// \param isFitMinimizer Flag that saves if the minimizer is used for structure optimization or
    ///                       parameter optimization.
    static std::unique_ptr<Minimizer> createAndParse(XML::Element minimizerElement, FitJob* job, int intervalForPrinting = 0,
                                                     bool isFitMinimizer = false);

    /// Function that creates a copy of this minimizer.
    virtual std::unique_ptr<Minimizer> clone() = 0;

protected:
    /// Computes the gradient of the objective function using finite differences.
    double numericGradient(const std::vector<double>& x, std::vector<double>& g);

protected:
    /// Pointer to the job this object belongs to.
    FitJob* _job;

    /// Calculates the value of the objective function to be minimized.
    std::function<double(const std::vector<double>&)> _func;

    /// Calculates the gradient of the objective function.
    std::function<double(const std::vector<double>&, std::vector<double>&)> _gradientFunc;

    /// Constraints.
    std::vector<BoundConstraints> _constraintTypes;
    std::vector<double> _lowerBounds;
    std::vector<double> _upperBounds;

    /// The current approximation to the solution.
    std::vector<double> x;

    /// The gradient of the objective function.
    std::vector<double> g;

    /// The best parameters and residual found so far.
    std::vector<double> _bestParameters;
    double _bestResidual;

    /// The value of the objective function;
    double f = 0.0;

    /// The 2-norm of the gradient.
    double _gradNorm2 = 0.0;

    /// Number of iterations performed so far.
    int _itercount = 0;

    /// Interval for printing optimized DOFs.
    int _intervalForPrinting;

    /// The limit for which the minimizer converges.
    double _convergenceThreshold = 1e-5;

    /// Maximum number of iterations for the minimizer.
    int _maximumNumberOfIterations = INT_MAX;

    /// The epsilon parameter for numeric calculation of gradient using finite differences.
    double _gradientEpsilon = 1e-6;

    /// The name of the minimizer as it will appear on the status line.
    FPString _name;

    /// Is this minimizer used for parameter fitting or for structure relaxation?
    bool _isFitMinimizer;
};

}  // End of namespace
