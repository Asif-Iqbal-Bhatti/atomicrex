///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "Minimizer.h"

namespace atomicrex {

class LBFGSMinimizer : public Minimizer
{
public:
    /// Constructor.
    ///
    /// \param job Pointer to the Fitjob
    /// \param intervalForPrinting Determines the frequency for printing the DOF list.
    /// \param isFitMinimizer Flag that saves if the minimizer is used for structure optimization or
    ///                       parameter optimization.
    LBFGSMinimizer(FitJob* job, int intervalForPrinting = 0, bool isFitMinimizer = false)
        : Minimizer(job, intervalForPrinting, "LBFGS", isFitMinimizer)
    {
    }

    /// Constructor that copies another minimizer.
    ///
    /// \param other LBFGS minimizer that is copied.
    LBFGSMinimizer(const LBFGSMinimizer& other);

    /// Initializes the minimizer by setting the objective function and the starting vector.
    /// Must be called once before entering the minimization loop.
    ///
    /// \param x0 An R-value reference to the starting vector.
    ///           The length of this vector determines the number of dimensions.
    ///           The function transfers the vector to internal storage. That means the
    ///           passed-in vector will no longer be valid after the function returns.
    /// \param func The object that computes the value of the objective function at a given point x.
    /// \param gradient An optional function object that computes the (analytic) gradient of the objective function
    ///                 at a given point x (and also the value of the objective function).
    ///                 If no gradient function is provided, and the minimization algorithm requires
    ///                 the gradient, the minimizer will compute it using finite differences by
    ///                 evaluating \a func several times.
    virtual void prepare(std::vector<double>&& x0, const std::function<double(const std::vector<double>&)>& func,
                         const std::function<double(const std::vector<double>&, std::vector<double>&)>& gradient =
                             std::function<double(const std::vector<double>&, std::vector<double>&)>()) override;

    /// Sets the constraints for variations of the parameters.
    /// Must be called after prepare() and before the minimization loop is entered.
    virtual void setConstraints(std::vector<BoundConstraints>&& constraintTypes, std::vector<double>&& lowerBounds,
                                std::vector<double>&& upperBounds) override;

    /// Performs one minimization iteration.
    virtual MinimizerResult iterate() override;

    /// Parses the minimizer's parameters from the XML file.
    /// 
    /// \param minimizerElement XML Element that contains the options for the minimizer.
    virtual void parse(XML::Element minimizerElement) override;

    /// Function that creates a copy of this minimizer.
    virtual std::unique_ptr<Minimizer> clone() override;

private:
    /// Evaluates the function at the current x and computes the gradient.
    void evaluate();

    // Variables passed to the L-BFGS-B Fortran routine:
    char task[60];
    char csave[60];
    int lsave[4];
    double dsave[29];
    int isave[44];
    double factr;
    std::vector<int> iwa;
    int m;
    double pgtol = 1e-5;
    std::vector<double> wa;
    int iprint;
};

}  // End of namespace
