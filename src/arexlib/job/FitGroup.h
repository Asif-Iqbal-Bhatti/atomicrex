///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "FitObject.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

class FitJob;  // Defined in FitJob.h

/**
 * A node in the hierarchical tree of structure and property groups.
 */
class FitGroup : public FitObject
{
public:
    /*************************************** Constructor ************************************/

    /// Constructor.
    FitGroup(const FPString& id, FitJob* job);

    /*************************************** Hierarchy ************************************/

    /// Returns the parent of this group (or NULL if this is the root node).
    FitGroup* parentGroup() const { return static_cast<FitGroup*>(parent()); }

    /// Creates a new child property group.
    FitGroup* createSubGroup(const FPString& id, FitJob* job);

    /*************************************** Parsing ************************************/

    /// Parses the contents of the <group> elements in the job file.
    virtual void parseElement(XML::Element groupElement, FitJob* job);
};

}  // End of namespace
