///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "../properties/FitProperty.h"
#include "../properties/DerivedProperty.h"
#include "FitGroup.h"
#include "../util/xml/XMLUtilities.h"
#include "../potentials/ParameterConstraint.h"

namespace atomicrex {

class Potential;        // Defined in Potential.h
class AtomicStructure;  // Defined in AtomicStructure.h
class DegreeOfFreedom;  // Defined in DegreeOfFreedom.h
class Minimizer;        // Defined in Minimizer.h
class ParameterConstraint;

class FitJob
{
public:
    /// Specifies presets for tolerance parameters
    enum TolerancePreset
    {
        Uniform,          ///< All tolerances are 1.0.
        Balanced,         ///< Specifies a balanced unit based assignment of tolerance parameters.
        AccurateEnergies  ///< Specifies a unit based assignment that emphasizes energies.
    };

public:
    /// Constructor
    FitJob();

    /// Destructor
    ~FitJob();

    /// Returns the user-defined name of this job.
    const FPString& name() const { return _name; }

    /// Returns the default tolerance for the given property based on the selected tolerance preset.
    double getDefaultPropertyTolerance(FitProperty* property);

    /// Returns the number of atom types used in this job.
    int numAtomTypes() const { return _natomtypes; }

    /// Adds a new atom type to the job.
    bool addAtomType(const FPString& name, double mass = 0.0, int atomicNumber = 0)
    {
        if(name.empty() || find(_atomTypeNames.begin(), _atomTypeNames.end(), name) != _atomTypeNames.end()) return false;
        _atomTypeNames.push_back(name);
        _atomTypeMasses.push_back(mass);
        _atomTypeAtomicNumbers.push_back(atomicNumber);
        _natomtypes = (int)_atomTypeNames.size();
        return true;
    }

    /// Returns the name of the i-th atom type.
    const FPString& atomTypeName(int i) const
    {
        BOOST_ASSERT(_natomtypes == _atomTypeNames.size());
        BOOST_ASSERT(i >= 0 && i < _atomTypeNames.size());
        return _atomTypeNames[i];
    }

    /// Returns the mass of the i-th atom type.
    /// May be zero if not set.
    double atomTypeMass(int i) const
    {
        BOOST_ASSERT(_natomtypes == _atomTypeMasses.size());
        BOOST_ASSERT(i >= 0 && i < _atomTypeMasses.size());
        return _atomTypeMasses[i];
    }

    /// Returns the atomic of the i-th atom type.
    /// May be zero if not set.
    int atomTypeAtomicNumber(int i) const
    {
        BOOST_ASSERT(_natomtypes == _atomTypeAtomicNumbers.size());
        BOOST_ASSERT(i >= 0 && i < _atomTypeAtomicNumbers.size());
        return _atomTypeAtomicNumbers[i];
    }

    /// Returns the index of the atom type.
    int atomTypeIndex(const FPString& s) const
    {
        auto iter = std::find(_atomTypeNames.begin(), _atomTypeNames.end(), s);
        BOOST_ASSERT(iter != _atomTypeNames.end());
        return iter - _atomTypeNames.begin();
    }

    /// Returns the maximum cutoff radius of all potentials used by this job.
    double maximumCutoff() const { return _maximumCutoff; }

    /// Returns the list of all potentials used in this job.
    const std::vector<Potential*>& potentials() const { return _potentials; }

    /// Returns the list of all atomic structures used for fitting.
    const std::vector<AtomicStructure*>& structures() const { return _structures; }

    /// Returns the list of all referenced atomic structures used for fitting.
    /// i.e. these structured need to be computed first as other structures depend on them.
    const std::vector<AtomicStructure*>& referencedStructures() const { return _referencedStructures; }

    /// Add a structure to the list of referenced structures.
    void addReferencedStructure(AtomicStructure* structure);

    /// Returns the list of all unreferenced atomic structures used for fitting.
    /// These structures can be computed without any particular order.
    const std::vector<AtomicStructure*>& unreferencedStructures() const { return _unreferencedStructures; }

    /// Add a structure to the list of unreferenced structures.
    void addUnreferencedStructure(AtomicStructure* structure) { _unreferencedStructures.push_back(structure); }

    /// Returns the list of all derived properties
    const std::vector<DerivedProperty*>& derivedProperties() { return _derivedProperties; }

    /// Returns the global list of active properties which are included in the fit and which need to be computed.
    const std::vector<FitProperty*>& fitProperties() { return _propertyList; }

    /// Returns the root node of the property group tree.
    FitGroup& rootGroup() { return _rootGroup; }

    /// Returns the atomic structure with the given ID.
    /// Return NULL if no such structure exists.
    ScalarFitProperty* getScalarFitProperty(const FPString& id);

    /// Returns the atomic structure with the given ID.
    /// Return NULL if no such structure exists.
    AtomicStructure* getAtomicStructure(const FPString& id);

    /// Returns the derived property with the given ID.
    /// Return NULL if no such property exists.
    DerivedProperty* getDerivedProperty(const FPString& id);

    /// Returns the deformation with the given id. Returns NULL if no such deformation has been defined.
    const Matrix3* lookupDeformation(const FPString& deformationId) const
    {
        auto d = _deformations.find(deformationId);
        if(d != _deformations.end())
            return &d->second;
        else
            return nullptr;
    }

    /// Returns whether fitting will be performed.
    bool fittingEnabled() const { return (bool)_fitMinimizer; }

    /// Returns the fit minimizer.
    Minimizer* fitMinimizer() { return _fitMinimizer.get(); }

    /// Returns the relaxation minimizer.
    Minimizer* relaxMinimizer() { return _relaxMinimizer.get(); }

    /// Performs the actual fitting by minimizing the residual.
    void performFitting();

    /**
       @brief Outputs the current values of all DOFs of the potentials.
       @param printFullDOFList Controls whether inactive DOFs are also printed. This option is used once before fitting to report
       the full list of DOFs.
    */
    void printPotentialDOFList(bool printFullDOFList);

    /// Prints the pseudo dofs that are dependent on other dofs and calculated using a ParameterConstraint.
    void printDependantDOFList();

    /**
       @brief Outputs the list of properties being fitted.
    */
    void printFittingProperties();

    /// Returns whether validation of the force/energy calculation routines has been requested by the user.
    bool potentialValidationEnabled() const { return _potentialValidationEnabled; }

    /// Performs a check of the energy/force calculation routines using numerical differentiation.
    void validatePotentials();

    /// Returns the total number of active degrees of freedom.
    int numActiveDOF() const { return _numActiveDOF; }

    /**
       @brief Packs the values of all degrees of freedom into a linear vector, which can be passed to the minimizer routine.
       @param destination A pointer to a contiguous array of doubles that will receive the values of all the degrees of freedom.
    */
    void packDOF(double* destination);

    /**
       @brief Packs the values of all degrees of freedom into a linear vector, which can be passed to the minimizer routine.
       @param destination A vector of doubles that will receive the values of all the degrees of freedom.
    */
    void packDOF(std::vector<double>& destination)
    {
        BOOST_ASSERT(destination.size() == numActiveDOF());
        packDOF(destination.data());
    }

    /**
       @brief Unpacks the values of all degrees of freedom from a linear vector.
       @param source A pointer to a contiguous array of doubles that contains the values of all the degrees of freedom.
    */
    void unpackDOF(const double* source);

    /**
       @brief Unpacks the values of all degrees of freedom from a linear vector.
       @param source A vector of doubles that contains the values of all the degrees of freedom.
    */
    void unpackDOF(const std::vector<double>& source)
    {
        BOOST_ASSERT(source.size() == numActiveDOF());
        unpackDOF(source.data());
    }

    /**
       @brief Packs the upper bounds of all degrees of freedom into a linear vector, which can be passed to the minimizer routine.
       @param destination A pointer to a contiguous array of doubles that will receive the values of all the degrees of freedom.
    */
    void packDOFupperBounds(double* destination);

    /**
       @brief Packs the upper bounds of all degrees of freedom into a linear vector, which can be passed to the minimizer routine.
       @param destination A vector of doubles that will receive the values of all the degrees of freedom.
    */
    void packDOFupperBounds(std::vector<double>& destination)
    {
        BOOST_ASSERT(destination.size() == numActiveDOF());
        packDOFupperBounds(destination.data());
    }

    /**
       @brief Packs the lower bounds of all degrees of freedom into a linear vector, which can be passed to the minimizer routine.
       @param destination A pointer to a contiguous array of doubles that will receive the values of all the degrees of freedom.
    */
    void packDOFlowerBounds(double* destination);

    /**
       @brief Packs the lower bounds of all degrees of freedom into a linear vector, which can be passed to the minimizer routine.
       @param destination A vector of doubles that will receive the values of all the degrees of freedom.
    */
    void packDOFlowerBounds(std::vector<double>& destination)
    {
        BOOST_ASSERT(destination.size() == numActiveDOF());
        packDOFlowerBounds(destination.data());
    }


public:
    /**
       @brief Parses the job description from the XML file.
       @param file Name of input file.
    */
    void parse(const FPString& file);

   /**
       @brief Define if output is written to a file.
       @param outputString Name of output file.
    */
    void setOutputFile(FPString &outputString);

    /**
       @brief Returns the atom type index of the species specified in the given element attribute.
       @details Throws exception if species has not been defined.
       @param element XML::Element object to be parsed
       @param attributeName The name of the attribute
       @returns atom type index as integer
    */
    int parseAtomTypeAttribute(XML::Element element, const char* attributeName);

    /**
       @brief Returns the atom type index of the species specified in the given element.
       @details Throws exception if species has not been defined.
       @param element XML::Element object to be parsed
       @param elementName The name of the element
       @returns atom type index as integer
    */
    int parseAtomTypeElement(XML::Element element, const char* elementName);

    /**
       @brief Returns the list of atom types corresponding to the species specified in the given element attribute.
       @details Throws exception if some species has not been defined.
       @param element XML::Element object to be parsed
       @param attributeName The name of the attribute
       @returns list of integers representing atom types
    */
    std::vector<int> parseAtomTypesAttribute(XML::Element element, const char* attributeName);

    /**
       @brief Returns the ScalarFitProperty specified in the given element attribute.
       @details Throws exception if property has not been defined.
       @param element XML::Element object to be parsed
       @param attributeName The name of the attribute
       @returns ScalarFitProperty specified in the element attribute
    */
    ScalarFitProperty* parseScalarFitPropertyAttribute(XML::Element element, const char* attributeName);

    /**
       @brief Calculates the total residual, i.e. the objective function to be minimized during fitting.
       @param norm The style to be used for computing the residual, see #FitProperty::ResidualNorm.
    */
    double calculateResidual(FitProperty::ResidualNorm norm = FitProperty::UserDefined);

    /// Prepares a list of all degrees of freedom and properties that are to be fitted.
    void prepareFitting();

    DegreeOfFreedom* DOFByPath(const FPString& pathId) const;

    FunctionBase* functionByPath(const FPString& pathId) const;

    void addParameterConstraint(ParameterConstraint* constraint);

private:
    /// Assigns absolute weights to the structural and the potential fit properties.
    void assignAbsoluteWeights();

    /**
       @brief Register a new structure with the job. It is added to the list of all atomic structures.
       @details This function is called by the AtomicStructure constructor.
       @param structure AtomicStructure object
    */
    void addStructure(AtomicStructure* structure);

    /**
       @brief Register a new derived property object with the job. It is added to the list of all derived properties objects.
       @details This function is called by the DerivedPropertyObject constructor.
       @param propertyObject DerivedPropertyObject to be registered
       @todo Maybe this function should be renamed to addDerivedPropertObject
    */
    void addDerivedProperty(DerivedProperty* derivedProperty);

private:
    /**
       @brief Parses the contents of the <atom-types> element in the job file.
       @param typesElement XML::Element to be parsed
    */
    void parseAtomTypesElement(XML::Element typesElement);

    /**
       @brief Parses the contents of the <potentials> element in the job file.
       @param potentialsElement XML::Element to be parsed
    */
    void parsePotentialsElement(XML::Element potentialsElement);

    /**
       @brief Parses the contents of the <fitting> element in the job file.
       @param fittingElement XML::Element to be parsed
    */
    void parseFittingElement(XML::Element fittingElement);

    /**
       @brief Parses the contents of the <deformations> element in the job file.
       @param deformationsElement XML::Element to be parsed
    */
    void parseDeformationsElement(XML::Element deformationsElement);

    /// parse content of ParameterConstraints element
    void parseParameterConstraintsElement(XML::Element constraintElement);

private:
    /// The user-defined name of this job.
    FPString _name;
   
    /// Output filestream.
    std::ofstream _outfile;
    /// Store a reference to original cout buffer
    std::streambuf* _stream_buffer_cout = std::cout.rdbuf();
    

    /// The number of atom types used in this job.
    int _natomtypes = 0;

    /// The identifiers/names assigned to the atom types.
    /// This is a 0-based array.
    std::vector<FPString> _atomTypeNames;

    /// The masses assigned to the atom types (may be zero if not defined).
    /// This is a 0-based array.
    std::vector<double> _atomTypeMasses;

    /// The atomic numbers assigned to the atom types.
    /// This is a 0-based array.
    std::vector<int> _atomTypeAtomicNumbers;

    /// The maximum cutoff radius of all potentials used in this job.
    double _maximumCutoff = 0.0;

    /// List of all potentials used in this job.
    std::vector<Potential*> _potentials;

    /// The root node of the group tree that contains the structures.
    FitGroup _rootGroup;

    /// Global list of all atomic structures contained in the group tree.
    std::vector<AtomicStructure*> _structures;

    /// Global list of all referenced atomic structures contained in the group tree.
    /// i.e. these structured need to be computed first as other structures depend on them.
    std::vector<AtomicStructure*> _referencedStructures;

    /// Global list of all unreferenced atomic structures contained in the group tree.
    /// These structures can be computed without any particular order.
    std::vector<AtomicStructure*> _unreferencedStructures;

    /// The global list of derived properties in this job.
    std::vector<DerivedProperty*> _derivedProperties;

    /// The global list of degrees of freedom of all potentials.
    std::vector<DegreeOfFreedom*> _completePotentialDOFList;

    /// The global list of degrees of freedom of all potentials that are being fitted.
    std::vector<DegreeOfFreedom*> _activePotentialDOFList;

    /// List of constraints applied to potential dofs
    std::vector<ParameterConstraint*> _parameterConstraintList;

    /// The total number of active degrees of freedom (# dimensions of the search space).
    /// This is the number of dimensions passed to the minimizer.
    size_t _numActiveDOF = 0;

    /// A linear vector that contains the values of all degrees of freedom being fitted.
    std::vector<double> _currentState;

    /// The global list of active properties which are included in the fit and which need to be computed.
    std::vector<FitProperty*> _propertyList;

    /// The minimizer to be used for fitting potentials.
    std::unique_ptr<Minimizer> _fitMinimizer;

    /// The minimizer to be used for relaxing structures.
    std::unique_ptr<Minimizer> _relaxMinimizer;

    /// List of deformations defined in the job file.
    std::map<FPString, Matrix3> _deformations;

    /// Flag that indicates whether validation of the force/energy calculation routines should be performed.
    /// The flag is set by the user in the job file.
    bool _potentialValidationEnabled = false;

    /// The selected preset for the property tolerances.
    TolerancePreset _tolerancePreset = Balanced;

    friend class FitGroup;
    friend class AtomicStructure;
    friend class DerivedProperty;
};

}  // End of namespace

#include "../structures/AtomicStructure.h"
#include "../potentials/Potential.h"
