///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "FitProperty.h"

namespace atomicrex {

/**
 * This class implements a property that consists of an array of Vector3 values,
 * which can be used for example to fit forces.
 */
class AtomVectorProperty : public FitProperty
{
public:
    /// Default constructor.
    AtomVectorProperty() {}

    /// Initialization constructor.
    AtomVectorProperty(const FPString& id, const FPString& units, FitJob* job) : FitProperty(id, units, job) {}

    /// Returns whether this property is calculated and displayed at the end of the fitting process.
    bool outputAllEnabled() const { return _outputAllEnabled; }

    /// Sets whether this property is calculated and displayed at the end of the fitting process.
    void setOutputAllEnabled(bool enable) { _outputAllEnabled = enable; }

    /// Allocates memory to store the property values for the given number of atoms (without ghost atoms).
    void setAtomCount(int numAtoms)
    {
        if(numAtoms != _computedValues.size()) {
            _computedValues.resize(numAtoms, Vector3::Zero());
            _targetValues.resize(numAtoms, Vector3::Zero());
            _hasTargetValues = false;
        }
    };

    /// Retrieve number of atoms.
    int atomCount() { return _computedValues.size(); };

    /// Returns the residual of this property used for fitting.
    virtual double residual(ResidualNorm norm) const override;

    /// Set the target values.
    void setTargetValue(int atomIndex, const Vector3& value)
    {
        BOOST_ASSERT(atomIndex >= 0 && atomIndex < _targetValues.size());
        _targetValues[atomIndex] = value;
        _hasTargetValues = true;
    };

    /// Returns the current computed values of the vector property.
    const std::vector<Vector3>& values() const { return _computedValues; }

    /// Returns the current computed values of the vector property.
    std::vector<Vector3>& values() { return _computedValues; }

    /// Returns the target values of the vector property.
    std::vector<Vector3>& targetValues() { return _targetValues; }
    const std::vector<Vector3>& targetValues() const { return _targetValues; }

    /// Outputs the current value of the property.
    virtual void print(MsgLogger& stream) override;

public:
    /// Parse the contents of the <property> element in the job file.
    virtual void parse(XML::Element propertyElement) override;

protected:
    /// This holds the computed values for the property.
    std::vector<Vector3> _computedValues;

    /// The target values.
    std::vector<Vector3> _targetValues;

    /// Controls whether all elements of the vector are to printed if output is enabled.
    bool _outputAllEnabled = false;

    /// Indicates that the target values have been set for this fit property.
    bool _hasTargetValues = false;
};

}  // End of namespace
