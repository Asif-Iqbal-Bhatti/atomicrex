///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FitProperty.h"
#include "../dof/DegreeOfFreedom.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Initializes the property object.
 ******************************************************************************/
void FitProperty::initialize(const FPString& id, const FPString& units, FitJob* job)
{
    _id = id;
    _units = units;
    _job = job;

    // Auto-initialize the tolerance of the property based on the selected global tolerance preset.
    setTolerance(job->getDefaultPropertyTolerance(this));
}

/******************************************************************************
 * Constructor.
 ******************************************************************************/
CoupledFitProperty::CoupledFitProperty(ScalarDOF& dof, const FPString& units, FitJob* job)
    : ScalarFitProperty(dof.id(), units, job), _dof(dof)
{
}

/******************************************************************************
 * Returns the residual of this property used for fitting
 ******************************************************************************/
double ScalarFitProperty::residual(ResidualNorm norm) const
{
    if(norm == UserDefined) norm = _residualNorm;

    if(norm == SquaredRelative) {
        if(_targetValue == 0.0)
            throw runtime_error(str(format("Target value 0 is not allowed for property '%1%' of structure '%2%' when residual "
                                           "style is 'squared-relative'.") %
                                    id() % parent()->id()));
        return square((_computedValue - _targetValue) / _targetValue);
    }
    else if(norm == Squared) {
        return square((_computedValue - _targetValue) / tolerance());
    }
    else if(norm == AbsoluteDiff) {
        return std::abs(_computedValue - _targetValue) / tolerance();
    }
    else
        return 0.0;
}

/******************************************************************************
 * Outputs the current value of the property.
 ******************************************************************************/
void ScalarFitProperty::print(MsgLogger& stream)
{
    stream << id() << ": " << _computedValue;
    if(!units().empty()) stream << " " << units();
    if(fitEnabled()) {
        stream << " (target: " << _targetValue;
        if(!units().empty()) stream << " " << units();
        stream << ")";
        stream << " Abs. weight: " << absoluteWeight();
        stream << "; Tolerance: " << tolerance();
        stream << "; Weighted residual: " << residual(_residualNorm) * absoluteWeight();
    }
}

/******************************************************************************
 * Lets the property object compute the current value of the property.
 ******************************************************************************/
void CoupledFitProperty::compute()
{
    ScalarFitProperty::compute();

    MsgLogger(debug) << "CoupledFitProperty::compute " << dof().getValue() << endl;

    // Adopt the current value of the DOF this property is coupled to.
    _computedValue = (double)dof();
}

/******************************************************************************
 * Outputs the current value of the property.
 ******************************************************************************/
void CoupledFitProperty::print(MsgLogger& stream)
{
    ScalarFitProperty::print(stream);
    if(dof().relax()) stream << " (relaxed)";
    if(dof().hasLowerBound() && dof().hasUpperBound())
        stream << " [" << dof().lowerBound() << ":" << dof().upperBound() << "]";
    else if(dof().hasLowerBound())
        stream << " [" << dof().lowerBound() << ":]";
    else if(dof().hasUpperBound())
        stream << " [:" << dof().upperBound() << "]";
}

/******************************************************************************
 * Parse the contents of the <property> element in the job file.
 ******************************************************************************/
void FitProperty::parse(XML::Element propertyElement)
{
    // Call base class.
    MsgLogger(debug) << "Parse <property> element (base class FitObject)." << endl;
    FitObject::parse(propertyElement);

    _tolerance = propertyElement.parseOptionalFloatParameterAttribute("tolerance", _tolerance);
    if(_tolerance <= 0)
        throw runtime_error(
            str(format("Tolerance must be non-zero and positive on line %1% of XML file") % propertyElement.lineNumber()));
}

/******************************************************************************
 * Parse the contents of the <property> element in the job file.
 ******************************************************************************/
void ScalarFitProperty::parse(XML::Element propertyElement)
{
    MsgLogger(debug) << "Parse <property> element (base class FitProperty)." << endl;
    FitProperty::parse(propertyElement);

    if(fitEnabled()) {
        // If fitting is enabled there needs to be a target value
        MsgLogger(debug) << "Parse target value." << endl;
        _targetValue = propertyElement.parseFloatParameterAttribute("target");
    }
    else if(propertyElement.hasAttribute("target") && !propertyElement.hasAttribute("fit")) {
        // Even if fitting has not been explicitly activated, if there is a target value we probably want to fit
        // However we need to make sure fit was not explicitly deactivated
        MsgLogger(debug) << "Parse target value and activate fitting." << endl;
        setFitEnabled(true);
        setOutputEnabled(propertyElement.parseOptionalBooleanParameterAttribute("output", true));
        _targetValue = propertyElement.parseFloatParameterAttribute("target");
    }

    MsgLogger(debug) << "Parse residual-style." << endl;
    FPString residualNorm = propertyElement.parseOptionalStringParameterAttribute("residual-style");
    if(!residualNorm.empty()) {
        if(residualNorm == "squared")
            _residualNorm = Squared;
        else if(residualNorm == "squared-relative")
            _residualNorm = SquaredRelative;
        else if(residualNorm == "absolute-diff")
            _residualNorm = AbsoluteDiff;
        else
            throw runtime_error(str(format("Invalid residual style in line %1% of XML file (acceptable values: squared, "
                                           "squared-relative, absolute-diff)") %
                                    propertyElement.lineNumber()));
    }
}

/******************************************************************************
 * Parse the contents of the <property> element in the job file.
 ******************************************************************************/
void CoupledFitProperty::parse(XML::Element propertyElement)
{
    ScalarFitProperty::parse(propertyElement);

    if(propertyElement.hasAttribute("equalto")) {
        dof().parseSetEqualToAttribute(propertyElement);
    }

    // Always activate relaxation if fit is enabled. Otherwise the value is just a constant anyway...
    if(fitEnabled()) dof().setRelax(true);

    if(propertyElement.hasAttribute("min")) dof().setLowerBound(propertyElement.parseFloatParameterAttribute("min"));
    if(propertyElement.hasAttribute("max")) dof().setUpperBound(propertyElement.parseFloatParameterAttribute("max"));
}
};
