///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "../dof/DegreeOfFreedom.h"

namespace atomicrex {

/**
   @brief This class defines a Lennard-Jones potential.
   @details The potential has the following functional form
   \f[
     E = \sum_{ij} V(r_{ij})
   \f]
   where
   \f[
     V(r) = 4 \varepsilon \left[ \left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^{6} \right]
   \f].
   Here, \f$\varepsilon\f$ and \f$\varepsilon\f$ are the two potential parameters that, respectively,
   define energy and length scale. The potential is truncated beyond a certain cutoff.

   The following code snippet, to be inserted in the @c potentials block,
   illustrates the definition of this potential type in the input file.
   @code
   <lennard-jones id="Lennard-Jones" species-a="*" species-b="*">
     <sigma>2.5</sigma>
     <epsilon>0.5</epsilon>
     <cutoff>10.0</cutoff>
     <fit-dof>
       <sigma/>
       <epsilon/>
     </fit-dof>
   </lennard-jones>
   @endcode
*/

class LennardJonesPotential : public Potential
{
public:
    /// Constructor.
    LennardJonesPotential(const FPString& id, FitJob* job);

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

public:
    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

    /// Produces an XML representation of the potential's current parameter values and DOFs that can be used as
    /// input in a subsequent fit job.
    virtual XML::OElement generateXMLDefinition() override;

private:
    ScalarDOF _sigma;    // The sigma parameter of the LJ potential.
    ScalarDOF _epsilon;  // The epsilon parameter of the LJ potential.
    double _cutoff;      // The local cutoff radius.
};

}  // End of namespace
