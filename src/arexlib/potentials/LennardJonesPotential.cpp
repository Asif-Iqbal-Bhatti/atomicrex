///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "LennardJonesPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
LennardJonesPotential::LennardJonesPotential(const FPString& id, FitJob* job)
    : Potential(id, job, "LJ"), _sigma("sigma"), _epsilon("epsilon")
{
    registerDOF(&_sigma);
    registerDOF(&_epsilon);
}

/******************************************************************************
* Computes the total energy of the structure.
******************************************************************************/
double LennardJonesPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    double prefactor1 = 4.0 * _epsilon * pow(_sigma, 12.0);
    double prefactor2 = 4.0 * _epsilon * pow(_sigma, 6.0);

    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij < _cutoff) {
                double rinv = 1.0 / rij;
                double rinv2 = rinv * rinv;
                double rinv6 = rinv2 * rinv2 * rinv2;
                totalEnergy += rinv6 * (prefactor1 * rinv6 - prefactor2);
            }
        }
    }
    return totalEnergy;
}

/******************************************************************************
* Computes the total energy and forces of the structure.
******************************************************************************/
double LennardJonesPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;
    vector<Vector3>& forces = structure.atomForces();
    std::array<double, 6>& virial = structure.virial();

    double prefactor1 = 4.0 * _epsilon * pow(_sigma, 12.0);
    double prefactor2 = 4.0 * _epsilon * pow(_sigma, 6.0);
    double prefactor3 = 12.0 * prefactor1;
    double prefactor4 = 6.0 * prefactor2;

    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij < _cutoff) {
                double rinv = 1.0 / rij;
                double rinv2 = rinv * rinv;
                double rinv6 = rinv2 * rinv2 * rinv2;
                totalEnergy += rinv6 * (prefactor1 * rinv6 - prefactor2);
                double forcelj = rinv6 * (prefactor3 * rinv6 - prefactor4);
                double fpair = forcelj * rinv2;
                BOOST_ASSERT(i < forces.size() && neighbor_j->localIndex < forces.size());
                Vector3 fvec = neighbor_j->delta * fpair;
                forces[i] += fvec;
                forces[neighbor_j->localIndex] -= fvec;
                virial[0] += neighbor_j->delta.x() * fvec.x();
                virial[1] += neighbor_j->delta.y() * fvec.y();
                virial[2] += neighbor_j->delta.z() * fvec.z();
                virial[3] += neighbor_j->delta.y() * fvec.z();
                virial[4] += neighbor_j->delta.x() * fvec.z();
                virial[5] += neighbor_j->delta.x() * fvec.y();
            }
        }
    }
    return totalEnergy;
}

/******************************************************************************
* Parses any potential-specific parameters in the XML element in the job file.
******************************************************************************/
void LennardJonesPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    _sigma = potentialElement.parseFloatParameterElement("sigma");
    _epsilon = potentialElement.parseFloatParameterElement("epsilon");
    _cutoff = potentialElement.parseFloatParameterElement("cutoff");
}

/******************************************************************************
 * Produces an XML representation of the potential's current parameter values
 * and DOFs that can be used as input in a subsequent fit job.
 ******************************************************************************/
XML::OElement LennardJonesPotential::generateXMLDefinition()
{
    XML::OElement root("lennard-jones");

    root.createFloatParameterElement("sigma", _sigma);
    root.createFloatParameterElement("epsilon", _epsilon);
    root.createFloatParameterElement("cutoff", _cutoff);

    // Generate <fit-dof> element.
    XML::OElement fitdofElement("fit-dof");
    for(DegreeOfFreedom* dof : DOF()) {
        XML::OElement dofElement = dof->generateXMLFitDefinition();
        if(dof->fitEnabled() || dofElement.attributeCount() > 1) fitdofElement.appendChild(dofElement);
    }
    if(fitdofElement.firstChildElement()) root.appendChild(fitdofElement);

    return root;
}
};
