///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "ParsedFunction.h"
#include "../../job/FitJob.h"
#include "../../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/*****************************************************************************
* Parses the XML definition of the function.
******************************************************************************/
void ParsedFunction::parse(XML::Element functionElement)
{
    // Get the name of the function input variable (e.g. 'x'), which is used in the math expression.
    _inputVarName = functionElement.parseStringParameterElement("input-var");

    // Get user-defined function expression and its derivative, which must be provided by the user.
    _expression = functionElement.parseStringParameterElement("expression");
    _expressionDerivative = functionElement.parseStringParameterElement("derivative");

    // Parse function parameters.
    for(XML::Element childElement = functionElement.firstChildElement("param"); childElement;
        childElement = childElement.nextSibling("param")) {
        FPString paramName = childElement.parseStringParameterAttribute("name");
        if(paramName == _inputVarName || DOFById(paramName) != nullptr || paramName == "cutoff")
            throw runtime_error(
                str(format("Name of function parameter in line %1% of XML file is invalid.") % childElement.lineNumber()));

        // Create a new degree of freedom.
        ScalarDOF dof;
        dof.setId(paramName);

        // Parse initial value of DOF.
        try {
            dof = std::stod(childElement.textContent());
        }
        catch(const std::exception& ex) {
            throw runtime_error(str(format("Invalid floating-point value in <%1%> element at line %2% of XML file.") %
                                    childElement.tag() % childElement.lineNumber()));
        }

        _params.push_back(dof);
    }

    // Register degrees of freedom.
    for(ScalarDOF& dof : _params) registerDOF(&dof);

    // Call base class.
    FunctionBase::parse(functionElement);

    // Verify first derivative.
    verifyDerivative();
}

/************************************************************
* This function is called by the DOFs of this object each time their value changes.
************************************************************/
void ParsedFunction::dofValueChanged(DegreeOfFreedom& dof)
{
    FunctionBase::dofValueChanged(dof);
    _parameterSet++;
}

/************************************************************
* Initializes the expression parser for the current thread.
************************************************************/
ParsedFunction::ThreadData* ParsedFunction::initializeEvaluator()
{
    // A table that stores the thread-local data for every ParsedFunction.
    static thread_local std::map<const ParsedFunction*, ThreadData> threadDataTable;

    // Look up the data structure for the current ParsedFunction.
    ThreadData* data;
    auto entry = threadDataTable.find(this);
    if(entry == threadDataTable.end())
        data = &threadDataTable[this];
    else
        data = &entry->second;

    // Re-initialize the parser if the input parameters have changed since last time.
    if(data->parameterSet != _parameterSet) {
        data->parameterSet = _parameterSet;
        try {
            data->parser.SetExpr(_expression);
            data->derivativeParser.SetExpr(_expressionDerivative);
            // Register input variable.
            data->parser.DefineVar(_inputVarName, &data->inputValue);
            data->derivativeParser.DefineVar(_inputVarName, &data->inputValue);

            // Pass DOF parameter values to function evaluator.
            for(const ScalarDOF& dof : _params) {
                data->parser.DefineConst(dof.id(), (double)dof);
                data->derivativeParser.DefineConst(dof.id(), (double)dof);
            }

            // Pass cutoff to function evaluator.
            data->parser.DefineConst("cutoff", cutoff());
            data->derivativeParser.DefineConst("cutoff", cutoff());

            // Pass another useful constant.
            data->parser.DefineConst("pi", M_PI);
            data->derivativeParser.DefineConst("pi", M_PI);

        }
        catch(mu::Parser::exception_type& ex) {
            data->parameterSet = 0;
            throw runtime_error(
                str(format("Failed to parse user-defined math expression of function '%1%': %2%") % id() % ex.GetMsg()));
        }
    }
    return data;
}

/************************************************************
* Evaluates the function at a position x.
************************************************************/
double ParsedFunction::evaluateInternal(double r)
{
    if(r < cutoff()) {
        ThreadData* data = initializeEvaluator();
        data->inputValue = r;
        try {
            return data->parser.Eval();
        }
        catch(mu::Parser::exception_type& ex) {
            throw runtime_error(str(format("Failed to evaluate user-defined math expression of function '%1%' at %2%=%3%: %4%") %
                                    id() % _inputVarName % r % ex.GetMsg()));
        }
    }
    else {
        return 0;
    }
}

/************************************************************
* Evaluates the function and its first derivative at a position x.
************************************************************/
double ParsedFunction::evaluateInternal(double r, double& deriv)
{
    if(r < cutoff()) {
        ThreadData* data = initializeEvaluator();
        data->inputValue = r;
        try {
            deriv = data->derivativeParser.Eval();
        }
        catch(mu::Parser::exception_type& ex) {
            throw runtime_error(str(
                format("Failed to evaluate user-defined math expression for first derivative of function '%1%' at %2%=%3%: %4%") %
                id() % _inputVarName % r % ex.GetMsg()));
        }
        try {
            return data->parser.Eval();
        }
        catch(mu::Parser::exception_type& ex) {
            throw runtime_error(str(format("Failed to evaluate user-defined math expression of function '%1%' at %2%=%3%: %4%") %
                                    id() % _inputVarName % r % ex.GetMsg()));
        }
    }
    else {
        deriv = 0;
        return 0;
    }
}
}
