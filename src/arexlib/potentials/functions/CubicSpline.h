///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../Atomicrex.h"

namespace atomicrex {

/**
 * A one-dimensional cubic spline function.
 *
 * The spline consists of N knots with coordinates (x,y)
 * ordered along the x-axis.
 *
 * The first derivative of the function must be explicitly
 * specified at the first and the last knot.
 */
class CubicSpline
{
public:
    /// Default constructor.
    CubicSpline() : _N(0) {}

    /// Initialization of the spline function.
    void init(int N, double derivStart, double derivEnd)
    {
        BOOST_ASSERT(N >= 2);
        _N = N;
        _derivStart = derivStart;
        _derivEnd = derivEnd;
        X.resize(N);
        Y.resize(N);
        Y2.resize(N);
    }

    /// Sets the position of a knot of the spline function.
    void setKnot(int i, double x, double y)
    {
        BOOST_ASSERT(i >= 0 && i < _N);
        X[i] = x;
        Y[i] = y;
    }

    /// Returns the number of knots.
    int knotCount() const { return _N; }

    /// Returns first derivative of spline function at knot 0.
    double derivStart() const { return _derivStart; }

    /// Returns first derivative of spline function at the last knot.
    double derivEnd() const { return _derivEnd; }

    /// Returns the X coordinate of the i-th knot.
    double knotX(int i) const
    {
        BOOST_ASSERT(i >= 0 && i < _N);
        return X[i];
    }

    /// Returns the Y coordinate of the i-th knot.
    double knotY(int i) const
    {
        BOOST_ASSERT(i >= 0 && i < _N);
        return Y[i];
    }

    /// Returns the second derivative at the i-th knot.
    double knotY2(int i) const
    {
        BOOST_ASSERT(i >= 0 && i < _N);
        return Y2[i];
    }

    /// Calculates the second derivatives of the cubic spline.
    void prepareSpline();

    /// Evaluates the spline function at position x.
    double eval(double x) const;

    /// Evaluates the spline function and its first derivative at position x.
    double eval(double x, double& deriv) const;

    /// Returns the cutoff radius of this spline function.
    double cutoff() const { return X.back(); }

    /// Create a Gnuplot script that displays the spline function.
    void writeGnuplot(const FPString& filename, const FPString& title) const;

private:
    int _N;                  // Number of spline knots
    std::vector<double> X;   // Positions of spline knots
    std::vector<double> Y;   // Function values at spline knots
    std::vector<double> Y2;  // Second derivatives at spline knots
    double _derivStart;      // First derivative at knot 0
    double _derivEnd;        // First derivative at knot (N-1)
};

}  // End of namespace
