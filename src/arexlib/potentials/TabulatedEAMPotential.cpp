///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "TabulatedEAMPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Parses the tabulated EAM functionals from the given file in "funcfl" format.
******************************************************************************/
void TabulatedEAMPotential::parseFuncflEAMFile(const FPString& filename)
{
    ifstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Failed to open EAM potential file: %1%") % filename));

    // Skip comment line.
    FPString commentLine;
    getline(stream, commentLine);

    // Read header info line.
    int element;
    double mass;
    double lattice_constant;
    FPString lattice_type;
    stream >> element >> mass >> lattice_constant >> lattice_type;

    // Read tabulation header line.
    int Nr, Nrho;
    double delta_r, delta_rho;
    stream >> Nrho >> delta_rho >> Nr >> delta_r >> _cutoff;
    if(Nrho < 2 || Nr < 2 || delta_rho <= 0.0 || delta_r <= 0.0 || _cutoff <= 0.0)
        throw runtime_error(str(format("Invalid EAM potential file: %1%") % filename));

    U.init(Nrho, delta_rho);
    z2r.init(Nr, delta_r);
    rho.init(Nr, delta_r);

    U.parse(stream);
    z2r.parse(stream);
    rho.parse(stream);

    for(int i = 0; i < Nr; i++) z2r.setKnot(i, square(z2r.knotY(i)) * 27.2 * 0.529);
    z2r.prepareSpline();
}

/******************************************************************************
* Parses the tabulated EAM functionals from the given file in "setfl" format.
******************************************************************************/
void TabulatedEAMPotential::parseSetflEAMFile(const FPString& filename)
{
    ifstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Failed to open EAM potential file: %1%") % filename));

    // Skip comment lines.
    FPString commentLine;
    getline(stream, commentLine);
    getline(stream, commentLine);
    getline(stream, commentLine);

    // Parse number of elements in file.
    int numElements;
    FPString line;
    getline(stream, line);
    istringstream linestream(line);
    linestream >> numElements;

    // Read tabulation header line.
    int Nr, Nrho;
    double delta_r, delta_rho;
    stream >> Nrho >> delta_rho >> Nr >> delta_r >> _cutoff;
    if(Nrho < 2 || Nr < 2 || delta_rho <= 0.0 || delta_r <= 0.0 || _cutoff <= 0.0)
        throw runtime_error(str(format("Invalid EAM potential file: %1%") % filename));

    if(numElements != 1)
        throw runtime_error(str(
            format("Invalid EAM potential file: %1%. Current implementation supports only single-element files.") % filename));

    // Read header info line.
    int element;
    double mass;
    double lattice_constant;
    FPString lattice_type;
    stream >> element >> mass >> lattice_constant >> lattice_type;

    U.init(Nrho, delta_rho);
    z2r.init(Nr, delta_r);
    rho.init(Nr, delta_r);

    U.parse(stream);
    rho.parse(stream);
    z2r.parse(stream);
}

/******************************************************************************
* Computes the total energy of the structure.
******************************************************************************/
double TabulatedEAMPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        if(isAtomTypeEnabled(structure.atomType(i)) == false) continue;
        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij < _cutoff) {
                rho_value += rho.eval(rij);

                double pair_pot = 0.5 * z2r.eval(rij) / rij;
                totalEnergy += pair_pot;
            }
        }
        totalEnergy += U.eval(rho_value);
    }
    return totalEnergy;
}

/******************************************************************************
* Computes the total energy and forces of the structure.
******************************************************************************/
double TabulatedEAMPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    vector<Vector3>& forces = structure.atomForces();
    std::array<double, 6>& virial = structure.virial();

    // Reset per-atom array.
    EAMAtomData* perAtomData = data.perAtomData<EAMAtomData>();
    memset(perAtomData, 0, sizeof(EAMAtomData) * structure.numLocalAtoms());

    // Compute charge density and compute embedding energies.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij < _cutoff) {
                double rho_value = rho.eval(rij);
                perAtomData[i].rho += rho_value;
                perAtomData[neighbor_j->localIndex].rho += rho_value;
            }
        }
    }

    // Compute U(rho) and U'(rho).
    for(int i = 0; i < structure.numLocalAtoms(); i++) {
        if(isAtomTypeEnabled(structure.atomType(i))) totalEnergy += U.eval(perAtomData[i].rho, perAtomData[i].Uprime);
    }

    // Compute two-body pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                double rho_prime;
                rho.eval(rij, rho_prime);
                int j = neighbor_j->localIndex;

                double z2r_deriv;
                double z2r_value = z2r.eval(rij, z2r_deriv);
                double pair_pot = z2r_value / rij;
                double pair_pot_deriv = (z2r_deriv - pair_pot) / rij;

                double fpair = 0;
                if(isAtomTypeEnabled(structure.atomType(i))) {
                    fpair += rho_prime * perAtomData[i].Uprime;
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                if(isAtomTypeEnabled(structure.atomType(j))) {
                    fpair += rho_prime * perAtomData[j].Uprime;
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                Vector3 fvec = neighbor_j->delta * (fpair / rij);
                forces[i] += fvec;
                forces[j] -= fvec;

                virial[0] -= neighbor_j->delta.x() * fvec.x();
                virial[1] -= neighbor_j->delta.y() * fvec.y();
                virial[2] -= neighbor_j->delta.z() * fvec.z();
                virial[3] -= neighbor_j->delta.y() * fvec.z();
                virial[4] -= neighbor_j->delta.x() * fvec.z();
                virial[5] -= neighbor_j->delta.x() * fvec.y();
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
* Parses any potential-specific parameters in the XML element in the job file.
******************************************************************************/
void TabulatedEAMPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // Parse external EAM filename.
    FPString filename = potentialElement.parsePathParameterElement("param-file");

    // Parse format type of external file.
    XML::Element paramFileElement = potentialElement.expectChildElement("param-file");
    FPString formatType = paramFileElement.parseOptionalStringParameterAttribute("format", "funcfl");

    // Read in tabulated functionals.
    if(formatType == "funcfl")
        parseFuncflEAMFile(filename);
    else if(formatType == "setfl")
        parseSetflEAMFile(filename);
    else
        throw runtime_error(
            str(format("Unknown EAM file format in line %1% of XML file: %2%") % paramFileElement.lineNumber() % formatType));
}
}
