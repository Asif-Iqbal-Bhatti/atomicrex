///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AtomicStructure.h"
#include "../dof/DegreeOfFreedom.h"
#include "../properties/FitProperty.h"

namespace atomicrex {

/**
   @brief This class defines a dimer.
   @details The dimer has one degree of freedom, namely the interatomic distance.
   @code
   <dimer-structure id="MY_DIMER">
     <atom-type-a> ELEMENT_NAME_1 </atom-type-a>
     <atom-type-b> ELEMENT_NAME_2 </atom-type-b>
     <atom-distance> ATOM_DISTANCE </atom_distance>
   </dimer-structure>
   @endcode
*/
class DimerStructure : public AtomicStructure
{
public:
    /// Constructor.
    DimerStructure(const FPString& id, FitJob* job);

    /// Updates the structure (atom positions, simulation cell, etc.)
    virtual void updateStructure() override;

    /// Parses any structure-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element structureElement) override;

private:
    /// The atom type of atom A.
    int _atomTypeA;

    /// The atom type of atom B.
    int _atomTypeB;

    /// DOF that controls the distance between the atoms.
    ScalarDOF _atomDistance;

    /// This property allows to compute the equilibrium distance.
    CoupledFitProperty _atomDistanceProperty;
};

}  // End of namespace
