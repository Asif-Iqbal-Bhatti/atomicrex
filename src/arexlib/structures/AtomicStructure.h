///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Atomicrex.h"
#include "NeighborList.h"
#include "../job/FitObject.h"
#include "../properties/FitProperty.h"
#include "../properties/AtomVectorProperty.h"
#include "../dof/AtomCoordinatesDOF.h"

namespace atomicrex {

class FitJob;  // Declared in FitJob.h

/**
* @brief      Base class for maintaining structures.
*
* @details    This class is used to store structure specific information
*             including atomic coordinates, neighbor lists, total energy,
*             volume, pressure etc.
*/
class AtomicStructure : public FitObject
{
public:
    /**
    * @brief Flags that indicate which parts of the structure need to be updated prior to the next energy calculation.
    */
    enum DirtyFlags
    {
        /// Neighbor lists require updating
        NEIGHBOR_LISTS = (1<<0),
        /// Ghost atom positions require updating
        ATOM_POSITIONS = (1<<1),
        /// Degree(s) of freedom requires updating
        STRUCTURE_DOF = (1<<2),
        /// Used to indicate to the potential that the structure has 
        /// changed since last energy/force calculation
        POTENTIAL_DATA = (1<<3),
    };

    /// Structure holding potential-specific data for each atomic structure.
    struct PerPotentialData {

        /// The potential this data structure is associated with.
        const Potential* pot;

        /// The atomic structure this data structure is associated with.
        const AtomicStructure* structure;

        /// The neighbor list requested by the potential for the current atomic structure.
        NeighborList neighborList;

        /// Memory buffer used for per-structure data used by the potential routine.
        std::vector<unsigned char> perStructureDataBuffer;

        /// Memory buffer for per-atom data used by the potential routine.
        std::vector<unsigned char> perAtomDataBuffer;

        /// Returns the typed pointer to the pre-structure data buffer.
        template<typename T>
        T* perStructureData() { return reinterpret_cast<T*>(perStructureDataBuffer.data()); }

        /// Returns a pointer to the memory buffer that stores temporary per-atom data used by the potential routines.
        template<typename T>
        T* perAtomData() { return reinterpret_cast<T*>(perAtomDataBuffer.data()); }
    };

    /**
    * @brief Possible formats for writing the structure to file
    */
    enum OutputFormat
    {
        POSCAR,
        LAMMPS
    };

public:
    /*************************************** Constructor / destructor ***********************************/

    AtomicStructure(const FPString& id, FitJob* job);

    /*************************************** Simulation cell geometry ************************************/

    /**
     @brief Set up the simulation cell.
     @param cellVectors cell metric (3x3 matrix)
     @param cellOrigin origin of cell (3-dim vector)
     @param pbc periodic boundary conditions in each direction
    */
    void setupSimulationCell(const Matrix3& cellVectors, const Point3& cellOrigin = Point3::Origin(),
                             const std::array<bool, 3>& pbc = std::array<bool, 3>{{true, true, true}});

    /**
       @brief Changes the cell and the atoms.
       @param newCell New Simulation cell
       @param scaleAtoms Determines if atom positions are scaled with respect to the change in cell dimensions
    **/
    void changeSimulationCell(const Matrix3& newCell, bool scaleAtoms);

    /**
       @brief Applies an affine transformation to the cell and the atoms.
       @param deformation defined 3x3 deformation matrix that is applied to the simulation cell
    **/
    void deformSimulationCell(const Matrix3& deformation);

    /**
       @brief Returns global simulation cell matrix.
       @return 3x3 matrix representing the simulation cell metric
    **/
    const Matrix3& simulationCell() const { return _simulationCell; }

    /**
       @brief Returns initial simulation cell matrix.
       @return 3x3 matrix representing the simulation cell metric
    **/
    const Matrix3& initialSimulationCell() const { return _initialSimulationCell; }

    /**
       @brief The origin point of the global simulation cell in world coordinates.
       @return 3-dimensional vector representing the origin of the simulation cell.
    **/
    const Point3& simulationCellOrigin() const { return _simulationCellOrigin; }

    /**
       @brief The inverse of the simulation cell matrix used to transform absolute coordinates to reduced coordinates.
       @return 3x3 matrix representing the reciprocal cell metric
    **/
    const Matrix3& reciprocalSimulationCell() const { return _reciprocalSimulationCell; }

    /**
        @brief Returns whether periodic boundary conditions are enabled in the given spatial dimension.
        @param dimension index to spatial dimension (0=x, 1=y, z=3)
        @return True/False if boundary condition is enabled/disabled.
     **/
    bool hasPBC(size_t dimension) const { return _pbc[dimension]; }

    /**
        @brief Returns the periodic boundary condition flags.
        @return 3-dimensional vector of True/False values.
     */
    const std::array<bool, 3>& pbc() const { return _pbc; }

    /*************************************** Coordinate utility functions ************************************/

    /// Determines the periodic image of the cell the given point is located in.
    Vector3I periodicImage(const Point3& p) const;

    /// Wraps a point to be inside the simulation cell if periodic boundary conditions are enabled.
    Point3 wrapPoint(Point3 p) const;

    /// Wraps a point given in reduced coordinated to be inside the simulation cell.
    Point3 wrapReducedPoint(Point3 p) const;

    /// Converts a point given in reduced cell coordinates to a point in absolute coordinates.
    Point3 reducedToAbsolute(const Point3& reducedPoint) const
    {
        return simulationCellOrigin() + (simulationCell() * (reducedPoint - Point3::Origin()));
    }

    /// Converts a point given in absolute coordinates to a point in reduced cell coordinates.
    Point3 absoluteToReduced(const Point3& worldPoint) const
    {
        return Point3::Origin() + (reciprocalSimulationCell() * (worldPoint - simulationCellOrigin()));
    }

    /// Converts a vector given in reduced cell coordinates to a vector in absolute coordinates.
    Vector3 reducedToAbsolute(const Vector3& reducedVec) const { return simulationCell() * reducedVec; }

    /// Converts a vector given in absolute coordinates to a point in vector cell coordinates.
    Vector3 absoluteToReduced(const Vector3& worldVec) const { return reciprocalSimulationCell() * worldVec; }

    /************************************************ Atoms ***********************************************/

    /**
       @brief Resizes the atoms array.
       @param numLocalAtoms new number of (local) atoms.
     */
    void setAtomCount(int numLocalAtoms);

    /// Returns the number of (real) atoms in the structure cell.
    int numLocalAtoms() const { return _numLocalAtoms; }

    /// Returns the number of ghost atoms in the structure cell.
    int numGhostAtoms() const { return _numGhostAtoms; }

    /// Returns the total number of atoms in the structure cell including real and ghost atoms.
    int numAtoms() const { return _numAtoms; }

    /// Returns the array of positions of all atoms (including ghosts) in the structure (const version).
    const std::vector<Point3>& atomPositions() const { return _atomPositions; }

    /// Returns the array of positions of all atoms (including ghosts) in the structure cell (non-const version).
    std::vector<Point3>& atomPositions() { return _atomPositions; }

    /**
       @brief Sets the array of positions of all atoms in the structure cell.
       @param newPositions vector of 3-dimensional vectors ("points") representing the atomic coordinates. Size must match the
       number of local atoms.
     */
    void setAtomPositions(const std::vector<Point3>& newPositions)
    {
        BOOST_ASSERT(newPositions.size() == numLocalAtoms());
        std::copy(newPositions.begin(), newPositions.end(), _atomPositions.begin());
        setDirty(ATOM_POSITIONS);
    }

    /// Returns the array of displacements of all atoms in the structure cell wrt a reference structure (const version).
    const std::vector<Point3>& atomDisplacements() const { return _atomDisplacements; }

    /// Returns the array of displacements of all atoms in the structure cell wrt a reference structure (non-const version).
    std::vector<Point3>& atomDisplacements() { return _atomDisplacements; }

    /// Returns the initial positions of the all real atoms they had at the beginning of the job.
    const std::vector<Point3>& initialAtomPositions() const { return _initialAtomPositions; }

    /// Returns the type of a i-th atom.
    int atomType(int i) const { return _atomTypes[i]; }

    /// Returns the array of types of all atoms in the cell (const version).
    const std::vector<int>& atomTypes() const { return _atomTypes; }

    /// Returns the array of types of all atoms in the cell (non-const version).
    std::vector<int>& atomTypes() { return _atomTypes; }

    /// Returns the array of IDs of the atoms in the cell (const version).
    const std::vector<int>& atomTags() const { return _atomTags; }

    /// Returns the array of IDs of the atoms in the cell (non-const version).
    std::vector<int>& atomTags() { return _atomTags; }

    /// Returns the array of force vectors (const version).
    const std::vector<Vector3>& atomForces() const { return _atomForcesProperty.values(); }

    /// Returns the array of force vectors (non-const version).
    std::vector<Vector3>& atomForces() { return _atomForcesProperty.values(); }

    /// Returns the property for the atomic force vectors.
    AtomVectorProperty& atomForcesProperty() { return _atomForcesProperty; }

    /// Returns an array of integer pairs that map the real atoms to the ghost atoms.
    const std::vector<std::pair<int, int>>& forwardMapping() const { return _forwardMapping; }

    /// Returns an array of integers that maps the ghost atoms to the real atoms.
    const std::vector<int>& reverseMapping() const { return _reverseMapping; }

    /************************************ Updating and modifying the structure ***************************************/

    /// Marks parts of the structure that must be updated before the next energy calculation.
    void setDirty(DirtyFlags flags) { _dirtyFlags |= flags; }

    /// Tests whether certain parts of the structure must be updated before the next energy calculation.
    bool isDirty(DirtyFlags parts) const { return (_dirtyFlags & parts) != 0; }

    /// Resets the dirty flags.
    void clearDirty(DirtyFlags parts) { _dirtyFlags &= ~parts; }

    /**
       @brief This callback function is called by the DOFs of the structure each time when their values changes.
       @warning This method should not be called by the user.
    */
    virtual void dofValueChanged(DegreeOfFreedom& dof) override
    {
        FitObject::dofValueChanged(dof);
        setDirty(STRUCTURE_DOF);
    }

    /**
       @brief Updates the structure, creates ghost atoms and builds neighbor lists.
    */
    virtual void updateStructure();

    /******************************************** Property calculation ***********************************************/

    /**
       @brief Computes all enabled properties of the structures.
       @param isFitting Indicates that the structure is included in the fitting data set.
    */
    virtual bool computeProperties(bool isFitting) override;

    /**
       @brief Computes the total energy and optionally the forces for this structure.
       @param computeForces Turns on the computation of forces.
       @param isFitting Indicates that the structure is included in the fitting data set.
       @param suppressRelaxation If True structure relaxation will be suppressed.
       @return A scalar representing the energy of the structure. If the forces are computed they are stored in the structure
       object.
       @see atomForcesProperty() for retrieving the atomic forces
    */
    virtual double computeEnergy(bool computeForces, bool isFitting, bool suppressRelaxation = false);

    /**
       @brief Relaxes the structural degrees of freedom such that the total energy is minimized.
       @param isFitting Indicates that the structure is included in the fitting data set.
    */
    virtual bool relax(bool isFitting);

    /******************************************** Input/output ***********************************************/

    /**
     @brief Exports the structure to a LAMMPS dump file.
     @param filename Name of the output file
     @param includeGhostAtoms If True ghost atoms used during computation of the energy will be included in the output
    */
    void writeToDumpFile(const FPString& filename, bool includeGhostAtoms = false) const;

    /**
     @brief Exports the structure to a POSCAR file.
     @param filename Name of the output file
     @param includeGhostAtoms If True ghost atoms used during computation of the energy will be included in the output
    */
    void writeToPoscarFile(const FPString& filename, bool includeGhostAtoms = false) const;

    /**
     @brief Exports the structure to file if a filename has been provided.
    */
    void writeToFile();

    /************************************* Access to calculated quantities *****************************************/

    /// Returns the total potential energy of this structure after computeEnergy() has been called.
    double totalEnergy() const { return _totalEnergy; }

    /// Returns the volume of the structure.
    double volume() const { return _totalVolumeProperty; }

    /// Returns the virial tensor computed by the force routine.
    const std::array<double, 6>& virial() const { return _virial; }

    /**
       @brief Returns a reference to the virial tensor.
       @details The virial tensor is returned as a six-dimensional vector. The indices of the vector follow the <a
       href="http://en.wikipedia.org/wiki/Voigt_notation">Voigt notation</a>.
    */
    std::array<double, 6>& virial() { return _virial; }

    /**
       @brief Returns a component of the pressure tensor.
       @param voigtIndex Index of the pressure tensor component in <a href="http://en.wikipedia.org/wiki/Voigt_notation">Voigt
       notation</a>
    */
    double pressureTensor(int voigtIndex) const
    {
        BOOST_ASSERT(voigtIndex >= 0 && voigtIndex < 6);
        return _pressureTensorProperty[voigtIndex];
    }

    /**
      @brief Returns the hydrostatic part of the pressure tensor.
      @details The function thus returns one third of the trace of the pressure tensor.
    **/
    double pressure() const { return _pressureProperty; }

    /************************************* Access to internal data structures *****************************************/

    /**
       @brief Returns the data record associated with this structure and the given potential.
    **/
    PerPotentialData& perPotentialData(const Potential* pot) {
        for(PerPotentialData& data : _perPotentialData)
            if(data.pot == pot) return data;

        BOOST_ASSERT(false);
        throw std::invalid_argument("AtomicStructure::perPotentialData() was called with an invalid potential pointer");
    }

public:
    /// Parses the structure-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element structureElement) override;

private:
    /**
       @brief Compute the elastic constants of the structure.
       @details This function triggers the computation of the elastic constants if the corresponding properties are enabled.
    **/
    void computeElasticConstants(bool isFitting);

    /// Indicate which parts of the structure must be updated before the next energy calculation.
    int _dirtyFlags;

    /// The global simulation cell matrix.
    Matrix3 _simulationCell;
    /// The initial simulation cell matrix.
    Matrix3 _initialSimulationCell;
    bool _saveInitialCell = true;
    /// The origin point of the global simulation cell in world coordinates.
    Point3 _simulationCellOrigin;
    /// The inverse of the simulation cell matrix used to transform absolute coordinates to reduced coordinates.
    Matrix3 _reciprocalSimulationCell;
    /// Indicates whether periodic boundary conditions are enabled for each of the three spatial directions.
    std::array<bool, 3> _pbc;

    /// The number of (real) atoms in the structure cell.
    int _numLocalAtoms;
    /// The number of ghost atoms.
    int _numGhostAtoms;
    /// The total number of atoms including real and ghost atoms.
    int _numAtoms;
    /// The neighbor list skin thickness.
    double _skinThickness;
    /// The positions of all atoms in the structure (including ghost atoms).
    std::vector<Point3> _atomPositions;
    /// The positions of the real atoms in the structure the last time the neighbor lists and ghost atoms have been generated.
    std::vector<Point3> _lastNeighborUpdatePositions;
    /// The simulation cell matrix last time the neighbor lists have been built.
    Matrix3 _lastSimulationCell;
    /// Stores the number of PBC ghost atom images in each spatial direction that were generated.
    Vector3I _ghostAtomImages;
    /// The displacements of the all atoms in the structure cell with respect to a reference structure.
    std::vector<Point3> _atomDisplacements;
    /// The initial positions of the all real atoms they had at the beginning of the job.
    std::vector<Point3> _initialAtomPositions;
    /// The types of all atoms in the cell (including ghost atoms).
    std::vector<int> _atomTypes;
    /// The IDs of the atoms in the cell (including ghost atoms).
    std::vector<int> _atomTags;
    /// Maps real atom indices to ghost atom indices.
    std::vector<std::pair<int, int>> _forwardMapping;
    /// Maps the ghost atoms to the real atoms.
    std::vector<int> _reverseMapping;
    /// Stores the PBC image for every ghost atom.
    std::vector<Vector3> _ghostAtomPBCImages;

    /// Itercount of last atom position update
    int _lastUpdate = 0;

    /// The path the structure should be saved to and the file format.
    FPString _outputFile = "";
    OutputFormat _outputFormat;

protected:
    /// The accumulated virial computed by the force routine.
    std::array<double, 6> _virial;

    /// The minimizer to be used for relaxing structures.
    std::unique_ptr<Minimizer> _relaxMinimizer;

    /// Stores the potential related data.
    std::vector<PerPotentialData> _perPotentialData;

    /// The total potential energy of this structure.
    double _totalEnergy;

    /// Output of the energy calculation that can be used for fitting.
    ScalarFitProperty _totalEnergyProperty;
    /// Output of the per atom energy calculation that can be used for fitting.
    ScalarFitProperty _atomicEnergyProperty;

    /// The total volume of the structure cell.
    ScalarFitProperty _totalVolumeProperty;
    /// The average volume per atom in the structure cell.
    ScalarFitProperty _atomicVolumeProperty;

    /// The components of the cell's pressure tensor.
    ScalarFitProperty _pressureTensorProperty[6];
    /// One third of the trace of the cell's pressure tensor.
    ScalarFitProperty _pressureProperty;

    /// The bulk modulus of the lattice.
    ScalarFitProperty _bulkModulusProperty;
    /// The elastic constants.
    ScalarFitProperty _elasticConstantProperties[21];

    /// The force vector.
    AtomVectorProperty _atomForcesProperty;

    /// This DOF encapsulates the atomic degrees of freedom.
    AtomCoordinatesDOF _atomCoordinatesDOF;
};

}  // End of namespace
