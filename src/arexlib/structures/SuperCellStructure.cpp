///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"
#include "SuperCellStructure.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Constructor.
******************************************************************************/
SuperCellStructure::SuperCellStructure(const FPString& id, FitJob* job)
    : AtomicStructure(id, job),
      _latticeParameter("lattice-parameter", 1, 0),
      _latticeParameterProperty(_latticeParameter, "A", job),
      _CtoAratio("ca-ratio", 1, 0),
      _CtoAratioProperty(_CtoAratio, FPString(), job)
{
    registerDOF(&_latticeParameter);
    registerDOF(&_CtoAratio);
    registerProperty(&_latticeParameterProperty);
    registerProperty(&_CtoAratioProperty);
}

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void SuperCellStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        setupSimulationCell(superCell());
        std::vector<std::pair<Point3, int>> atoms = superCellAtoms();
        setAtomCount(atoms.size());
        auto pos = atomPositions().begin();
        auto type = atomTypes().begin();
        for(const auto& atom : atoms) {
            *pos++ = atom.first;
            *type++ = atom.second;
        }

        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    AtomicStructure::updateStructure();
}

/******************************************************************************
* Returns the geometry of the selected lattice unit cell.
******************************************************************************/
Matrix3 SuperCellStructure::unitCell() const
{
    if(_latticeType == LATTICE_FCC || _latticeType == LATTICE_BCC) {
        return Matrix3(_latticeParameter, 0, 0, 0, _latticeParameter, 0, 0, 0, _latticeParameter);
    }
    else if(_latticeType == LATTICE_HCP) {
        const double angle = 60.0 * FLOATTYPE_PI / 180.0;
        const double a = cos(angle);
        const double b = sin(angle);
        return Matrix3(_latticeParameter, -_latticeParameter * a, 0, 0, _latticeParameter * b, 0, 0, 0,
                       _latticeParameter * _CtoAratio);
    }
    else {
        throw runtime_error(str(format("Invalid lattice type for super-cell structure '%1%'.") % id()));
    }
}

/******************************************************************************
* Returns the geometry of the super cell.
******************************************************************************/
Matrix3 SuperCellStructure::superCell() const { return unitCell() * Matrix3(_cellSize, 0, 0, 0, _cellSize, 0, 0, 0, _cellSize); }

/******************************************************************************
* Returns the atoms in the lattice unit cell (reduced coordinates).
******************************************************************************/
std::vector<std::pair<Point3, int>> SuperCellStructure::unitCellBasis() const
{
    std::vector<std::pair<Point3, int>> basis;
    if(_latticeType == LATTICE_FCC) {
        basis.push_back(std::make_pair(Point3(0.0, 0.0, 0.0), _atomTypeA));
        basis.push_back(std::make_pair(Point3(0.5, 0.5, 0.0), _atomTypeA));
        basis.push_back(std::make_pair(Point3(0.0, 0.5, 0.5), _atomTypeA));
        basis.push_back(std::make_pair(Point3(0.5, 0.0, 0.5), _atomTypeA));
    }
    else if(_latticeType == LATTICE_HCP) {
        basis.push_back(std::make_pair(Point3(1.0 / 3.0, 2.0 / 3.0, 0.75), _atomTypeA));
        basis.push_back(std::make_pair(Point3(2.0 / 3.0, 1.0 / 3.0, 0.25), _atomTypeA));
    }
    else if(_latticeType == LATTICE_BCC) {
        basis.push_back(std::make_pair(Point3(0.0, 0.0, 0.0), _atomTypeA));
        basis.push_back(std::make_pair(Point3(0.5, 0.5, 0.5), _atomTypeA));
    }
    return basis;
}

/******************************************************************************
* Builds a list of atoms in the super cell.
******************************************************************************/
std::vector<std::pair<Point3, int>> SuperCellStructure::superCellAtoms() const
{
    std::vector<std::pair<Point3, int>> basis = unitCellBasis();
    std::vector<std::pair<Point3, int>> atomList(basis.size() * _cellSize * _cellSize * _cellSize);
    Matrix3 cell = unitCell();
    auto outAtom = atomList.begin();
    for(int x = 0; x < _cellSize; x++) {
        for(int y = 0; y < _cellSize; y++) {
            for(int z = 0; z < _cellSize; z++) {
                for(const auto& basisAtom : basis) {
                    outAtom->first = cell * Point3(basisAtom.first.x() + x, basisAtom.first.y() + y, basisAtom.first.z() + z);
                    outAtom->second = basisAtom.second;
                    ++outAtom;
                }
            }
        }
    }
    return atomList;
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void SuperCellStructure::parse(XML::Element structureElement)
{
    AtomicStructure::parse(structureElement);

    // Parse parameters.
    MsgLogger(debug) << "Parse <super-cell> parameters." << endl;
    _atomTypeA = job()->parseAtomTypeElement(structureElement, "atom-type-A");
    FPString latticeTypeName = structureElement.parseStringParameterElement("lattice-type");
    if(latticeTypeName == "bcc")
        _latticeType = LATTICE_BCC;
    else if(latticeTypeName == "fcc")
        _latticeType = LATTICE_FCC;
    else if(latticeTypeName == "hcp")
        _latticeType = LATTICE_HCP;
    else
        throw runtime_error(
            str(format("The lattice type '%1%' selected for super-cell structure '%2%' is unknown.") % latticeTypeName % id()));
    _cellSize = structureElement.parseIntParameterElement("cell-size");
    _latticeParameter.setInitialValue(structureElement.parseFloatParameterElement("lattice-parameter"));
    _CtoAratio.setInitialValue(structureElement.parseOptionalFloatParameterElement("ca-ratio", 1));
}
}
