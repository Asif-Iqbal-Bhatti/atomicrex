///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "SuperCellStructure.h"
#include "../dof/DegreeOfFreedom.h"
#include "../properties/FitProperty.h"

namespace atomicrex {

/**
   @brief This class defines point defects (vacancies, interstitials, substitutionals) in a super-cell.
   @details The structure class is based on the SuperCellStructure class, which provides corresponding degrees of freedom
   that control the construction of the perfect crystal lattice. The user has to specify a list of
   atoms to remove, substitute or add in the perfect crystal.
   The following code snippet exemplifies the definition of the lattice structure in the input file.
   The reference energy can either be a value or a already defined structure.
   Atom positions of the interstitial atoms to be inserted are specified in reduced coordinates of the lattice unit cell.
   @code
   <point-defect id="MY_STRUCTURE_NAME">
     <atom-type-A> ELEMENT_NAME_1 </atom-type-A>
     <lattice-type> LATTICE_TYPE </lattice-type>
     <lattice-parameter> LATTICE_PARAMETER </lattice-parameter>
     <cell-size> CELL_SIZE </cell-size>
     <relax-dof>
       <atom-coordinates fit='true'/>
     </relax-dof>
     <vacancy index="ATOM_TO_REMOVE" />
     <substitutional index="ATOM_TO_REPLACE" atom-type="ELEMENT_NAME_2" />
     <interstitial x="X_POSITIONS" y="Y_POSITIONS" z="Z_POSITIONS" atom-type="ELEMENT_NAME_2" />
     <properties>
       <lattice-parameter equalto="REFERNCE_STRUCTURE_A_ID.lattice-parameter"/>
     </properties>
   </point-defect>
   @endcode
*/
class PointDefectStructure : public SuperCellStructure
{
public:
    /// Constructor.
    PointDefectStructure(const FPString& id, FitJob* job) : SuperCellStructure(id, job) {}

    /// Updates the structure (atom positions, simulation cell, etc.)
    virtual void updateStructure() override;

    /// Parses any structure-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element structureElement) override;

private:
    /// Vacancies
    std::vector<int> _vacancyIndices;

    /// Substitutionals
    std::vector<int> _substitutionalIndices;
    std::vector<int> _substitutionalTypes;

    /// Interstitials
    std::vector<Point3> _interstitialPositions;
    std::vector<int> _interstitialTypes;
};

}  // End of namespace
