# Base image
FROM fedora:latest

# Upgrade the base packages
RUN yum update -y
RUN yum upgrade -y

# Install packages that are needed for building the code
RUN yum install -y cmake \
                    make \
                    automake \
                    gcc \
                    gcc-c++ \
                    kernel-devel \
                    gcc-gfortran

RUN yum install -y boost-devel

RUN yum install -y libxml2-devel \
                    nlopt-devel \
                    muParser-devel

# - the vim-common package provides the xxd program,
# which is used during compilation
RUN yum install -y vim-common

# - packages for Python3
RUN yum install -y python3-devel \
                    python3-numpy \
                    python3-sphinx \
                    python3-yaml \
                    python3-pip \
                    python3-scipy

# - packages for Python2
RUN yum install -y python-devel \
                    python-numpy \
                    python-sphinx \
                    python-yaml \
                    python-pip \
                    python-scipy

# - packages needed for building the homepage
RUN yum install -y wget \
                    unzip

# Set up additional Python2 packages via pip
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install ase \
                coverage \
                flake8 \
                matplotlib \
                sphinx-rtd-theme

# Set up additional Python3 packages via pip
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install ase \
                 coverage \
                 flake8 \
                 matplotlib \
                 sphinx-rtd-theme
