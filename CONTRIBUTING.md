Contribution guidelines
=======================


C++
---

At the moment, we have unfortunately not yet established a unified
coding style. In general, we lean to a more concise style that is in
the spirit of [K&R](https://en.wikipedia.org/wiki/Indent_style) and
[pep7](https://www.python.org/dev/peps/pep-0007/).

Any functions/functionality *must* be properly documented. The
API documentation is generated using
[doxygen](http://www.stack.nl/~dimitri/doxygen/). You should therefore
include comment blocks that document your code and are formatted to
comply with the doxygen markup style. While examples can of course be
found in the code, more extensive examples can be found in the
[documentation](http://www.atomicrex.org/).


Python
------

Code should be [pep8](https://www.python.org/dev/peps/pep-0008/)
compliant and pass
[pyflakes](https://pypi.python.org/pypi/pyflakes). (Eventually,
pep8/pyflakes will be added to the CI, at which point code *must* be
compliant.)

Any functions/functionality *must* be properly documented. This
includes [docstrings](https://en.wikipedia.org/wiki/Docstring) for
functions, classes, and modules that clearly describe the task
performed, the interface (where necessary), the output, and if
possible an example. atomicrex uses [NumPy Style Python
Docstrings](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html).

When in doubt ask the main developers. Also [the coding conventions
from
ASE](https://wiki.fysik.dtu.dk/ase/development/python_codingstandard.html)
provide useful guidelines.


Commits/issues/merge requests
-----------------------------

When writing commit messages, generating issues, or submitting merge
requests, please use the following codes to identify the category of
your commit/issue/merge request.

* API: an (incompatible) API change  
* BLD: change related to building  
* BUG: bug fix  
* DEP: deprecate something, or remove a deprecated object  
* DEV: development tool or utility  
* DOC: documentation  
* ENH: enhancement  
* MAINT: maintenance commit (refactoring, typos, etc.)  
* REV: revert an earlier commit  
* STY: style fix (whitespace, PEP8)  
* TST: addition or modification of tests  
* REL: related to releases  

The first line should not exceed 78 characters. If you require more
space, insert an empty line after the "title" and add a longer message
below. In this message, you should again limit yourself to 78
characters *per* line.

Hint: If you are using emacs you can use `Meta`+`q`
[shortcut](https://shortcutworld.com/en/Emacs/23.2.1/linux/all) to
"reflow the text". In sublime you can achieve a similar effect by using
`Alt`+`q`.
