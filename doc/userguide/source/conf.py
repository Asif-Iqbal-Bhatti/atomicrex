#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath('../../../examples'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'sphinx.ext.autosummary',
    'sphinx.ext.napoleon',
    'sphinx_sitemap'
]

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
pygments_style = 'sphinx'
todo_include_todos = True
autodoc_default_flags = ['no-private-members']
autosummary_generate = True

project = u'atomicrex'
copyright = u'2018, Alexander Stukowski and Paul Erhart'

site_url = 'https://atomicrex.org/'
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
exclude_patterns = []

html_logo = "_static/logo.png"
html_favicon = "_static/logo.ico"
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_static_path = ['_static']
html_theme_options = {'display_version': False}
html_context = {
    'software':
        [('atomicrex',
          'https://atomicrex.org/',
          'interatomic potential construction'),
         ('dynasor',
          'https://dynasor.materialsmodeling.org/',
          'dynamical structure factors from MD'),
         ('hiPhive',
          'https://hiphive.materialsmodeling.org/',
          'anharmonic force constant potentials'),
         ('icet',
          'https://icet.materialsmodeling.org/',
          'cluster expansions'),
         ('libvdwxc',
          'https://libvdwxc.org/',
          'library for van-der-Waals functionals'),
         ('storq',
          'https://storq.materialsmodeling.org/',
          'high-throughput submission system'),
         ('vcsgc-lammps',
          'https://vcsgc-lammps.materialsmodeling.org/',
          'Monte Carlo simulations with lammps'),
         ]}
htmlhelp_basename = 'atomicrexdoc'

# Options for LaTeX output
_PREAMBLE = r"""
\usepackage{amsmath,amssymb}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\argmin}{\arg\!\min}
"""
latex_elements = {
    'preamble': _PREAMBLE,
}
latex_documents = [
  ('index', 'atomicrex.tex', u'atomicREX Documentation',
   u'Alexander Stukowski and Paul Erhart', 'manual'),
]

# -- Options for manual page output ---------------------------------------
man_pages = [
    ('index', 'atomicrex', u'atomicREX Documentation',
     [u'Alexander Stukowski and Paul Erhart'], 1)
]

# -- Options for Texinfo output -------------------------------------------
texinfo_documents = [
  ('index', 'atomicrex', u'atomicREX Documentation',
   u'Alexander Stukowski and Paul Erhart', 'atomicrex', 'the atomic potential engineering tool.',
   'Scientific'),
]
