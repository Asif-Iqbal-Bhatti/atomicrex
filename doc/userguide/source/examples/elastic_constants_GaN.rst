.. _example_elastic_constants_GaN:

.. index::
   single: Analytic bond-order potential (ABOP); example
   single: Examples; Analytic bond-order potential (ABOP)
   single: Examples; Elastic constants
   single: Elastic constants; example
   single: Examples; Multi-component system

Elastic properties of gallium nitride
=================================================================================================

This example demonstrates the relaxation and property evaluation for
compound structures. In particular, it shows how to handle the
computation of :ref:`elastic constants <elastic_constants>` for
structure with internal relaxation due to deformation, such as
zincblende and wurtzite. The example employs the :ref:`analytic
bond-order potential (ABOP) <abop_potential>` from [NorAlbErh03]_.


Location
------------------

`examples/elastic_constants_GaN`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_GaN/main.xml
       :linenos:
       :language: xml

* `GaN.tersoff`: initial parameter set in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/elastic_constants_GaN/GaN.tersoff
       :linenos:
       :language: text


Output
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/elastic_constants_GaN/reference_output/log
       :linenos:
       :language: text

Other files
------------------

* `lammps_runs/data.lammps`: elastic constants computed using `Lammps
  <http://lammps.sandia.gov/>`_; the original runs can be found in the
  tgz-archive `wurtzite.tgz` in the `lammps_runs` subdirectory.

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_GaN/lammps_runs/data.lammps
       :linenos:
       :language: text
