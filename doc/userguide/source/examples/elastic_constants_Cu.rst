.. _example_elastic_constants_Cu:

.. index::
   single: Embedded atom method (EAM); example
   single: Examples; Embedded atom method (EAM)
   single: Examples; Elastic constants
   single: Elastic constants; example


Elastic properties of copper
============================

This example demonstrates the computation of elastic constants using
an :ref:`embedded atom method (EAM) potential <eam_potential>` based
on :ref:`user defined functions <user_defined_functions>`. The
potential form and parameters have been taken from [MisMehPap01]_. The
example also illustrates the use of :ref:`XML Inclusions
<xml_inclusions>`.

In particular this example provides a demonstration of the calculation
of both "clamped ion" and "relaxed ion" :ref:`elastic constants
<elastic_constants>` for a hexagonal lattice structure.


Location
--------

`examples/elastic_constants_Cu`

Input files
-----------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_Cu/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input file
  via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_Cu/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input file via
  :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_Cu/structures.xml
       :linenos:
       :language: xml


Output
------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/elastic_constants_Cu/reference_output/log
       :linenos:
       :language: text

Other files
-----------

* `lammps_runs/data.lammps`: elastic constants computed using `Lammps
  <http://lammps.sandia.gov/>`_; the original runs can be found in the
  tgz-archives in the `lammps_runs` subdirectory.

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/elastic_constants_Cu/lammps_runs/data.lammps
       :linenos:
       :language: text
