.. _example_derived_properties:

.. index::
   single: Analytic bond-order potential (ABOP); example
   single: NLopt minimizer; example
   single: Examples; Analytic bond-order potential (ABOP)
   single: Examples; NLopt minimizer


Computing a derived property using a point defect structure
======================================================================

This example demonstrates the use of a :ref:`derived property
<derived_property>` and a :ref:`point defect structure <point_defect_structures>`
to compute the vacancy formation energy of body centered cubic (BCC) Fe.

Location
------------------

`examples/derived_properties`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/derived_properties/main.xml
       :linenos:
       :language: xml

* `Fe.tersoff`: initial parameter set in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/derived_properties/Fe.tersoff
       :linenos:
       :language: text

Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/derived_properties/reference_output/log
       :linenos:
       :language: text

* For reference the code also writes the defect structure in `POSCAR
  file format.
  <http://cms.mpi.univie.ac.at/vasp/vasp/POSCAR_file.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/derived_properties/reference_output/POSCAR.defect-structure
       :linenos:
       :language: text
