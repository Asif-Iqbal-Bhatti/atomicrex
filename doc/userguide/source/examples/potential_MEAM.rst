.. _example_potential_meam:

.. index::
   single: Modified embedded atom method (MEAM); example
   single: Examples; Modified embedded atom method (MEAM)
   single: Examples; User defined functions
   single: User defined functions; example
   single: Examples; extended markup language (XML) inclusions
   single: extended markup language (XML); Inclusions; example


Modified embedded atom method (MEAM) potential with user defined functions
=================================================================================================

This example demonstrates the use of the :ref:`modified embedded atom
method (MEAM) potential <meam_potential>` routine together with
:ref:`user defined functions <user_defined_functions>`. The potential
form and parameters have been taken from [LenSadAlo00]_. The example
also illustrates the use of :ref:`XML Inclusions
<xml_inclusions>`. Note that the potential is merely evaluated for a
couple of simple lattice structures and none of the parameters are
fitted.

Location
------------------

`examples/potential_MEAM`

Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_MEAM/main.xml
       :linenos:
       :language: xml

* `potential.xml`: potential parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_MEAM/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input file via
  :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/potential_MEAM/structures.xml
       :linenos:
       :language: xml


Output (files)
------------------

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/potential_MEAM/reference_output/log
       :linenos:
       :language: text
