.. index::
   single: Pair potential; example
   single: Examples; Pair potential


Fitting a pair potential with a user defined function
===============================================================

This example demonstrates the (ab)use of the :ref:`embedded atom
method (EAM) potential <eam_potential>`. A pair potential of the
generalized Morse form is fitted to the lattice constant, bulk
modulus, and cohesive energy of copper in the face-centered cubic
(FCC) crystal structure. The example also illustrates the use of
:ref:`XML Inclusions <xml_inclusions>`. Note that the final potential
is not particularly good, which is unsurprising given the fact that a
pair potential form is being used with a limited number of parameters.

Location
------------------

`examples/fitting_pair_potential`


Input files
------------------

* `main.xml`: main input file

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/main.xml
       :linenos:
       :language: xml

* `potential.xml`: initial parameter set (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/potential.xml
       :linenos:
       :language: xml

* `structures.xml`: input structures (included in main input
  file via :ref:`XML Inclusions <xml_inclusions>`)

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/structures.xml
       :linenos:
       :language: xml


Output (files)
------------------

* `Cu_potential.V.table`: pair potential suitable for plotting

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/reference_output/Cu_potential.V.table
       :linenos:
       :language: text

* The final properties (as well as parameters) are written to standard
  output.

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/reference_output/log
       :linenos:
       :language: text


Other files
------------------

* `pairpotential_reference.table`: the pair potential
  from the :ref:`embedded atom method (EAM) potential <eam_potential>`
  presented in [MisMehPap01]_

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/reference_output/pairpotential_reference.table
       :linenos:
       :language: text

* `plot_pairpotential.py`: a python script that plots a comparison
  between the pair potential fitted here and the pair potential from
  the EAM potential in [MisMehPap01]_

  .. container:: toggle

    .. container:: header

      ..

    .. literalinclude:: ../../../../examples/fitting_pair_potential/reference_output/plot_pairpotential.py
       :linenos:

.. figure::  ../../../../examples/fitting_pair_potential/reference_output/pairpotential.svg
    :width: 400
    :align: center

    Pair interaction from a pure pair and an EAM potential
    [MisMehPap01]_.
