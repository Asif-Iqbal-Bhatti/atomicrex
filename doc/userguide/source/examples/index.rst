.. _examples:

Examples
********

The :program:`atomicrex` root directory contains an `examples`
subdirectory with several complete runs including both input and
reference output datasets. These cases can be used for testing the
code, e.g., during debugging or after compilation on a new
platform. The examples can also serve as tutorials that demonstrate
the use of different features, potentials, structures, etc. The
following sections provide an overview of several key examples.

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 1

   potential_EAM
   potential_MEAM
   potential_SW

   fitting_pair_potential
   fitting_EAM_potential
   fitting_ABOP_potential
   fitting_Spa_minimizer
   fitting_NLopt_minimizer

   derived_properties
   relaxing_user_structure
   imposing_a_deformation

   elastic_constants_Cu
   elastic_constants_GaN
   elastic_constants_Si
