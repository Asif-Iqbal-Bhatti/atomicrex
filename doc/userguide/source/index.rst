.. raw:: html

  <p>
  <a href="https://gitlab.com/atomicrex/atomicrex/commits/master"><img alt="build status" src="https://gitlab.com/atomicrex/atomicrex/badges/master/build.svg"></a>
  </p>


:program:`atomicrex` — A tool for the construction of interaction models
************************************************************************

:program:`atomicrex` is a versatile tool for the construction of advanced atomistic models. It is written in C++ and Python.

It was primarily developed to fit interatomic potential models.
Thanks to its flexible generic structure its application range, however, is larger.
In a general sense, it allows one to develop models that describe a given property as a function of an atomic (or atom-like) configuration.
The property in question can be scalar or vectorial in nature, and could represent e.g., total energies and forces.

**At the moment, we are no longer actively developing :program:`atomicrex`.
You can of course still use :program:`atomicrex` as well as modify and extend the source code to fit your needs.**

The source code of :program:`atomicrex` is hosted on `gitlab  <https://gitlab.com/atomicrex/atomicrex>`_.
If you use :program:`atomicrex` in your research please include the following citation in publications or presentations:

* A. Stukowski, E. Fransson, M. Mock, and P. Erhart,
  *Atomicrex - a general purpose tool for the construction of atomic interaction models*,
  Modelling Simul. Mater. Sci. Eng. **25**, 055003 (2017),
  `doi: 10.1088/1361-651X/aa6ecf <http://dx.doi.org/10.1088/1361-651X/aa6ecf>`_

Contents
-----------

.. toctree::
   :glob:
   :maxdepth: 2

   installation
   overview
   general_usage
   fitting
   potentials/index
   structures
   properties
   examples/index
   python/index
   advanced_topics/index
   scripts
   contributions
   credits
   bibliography
   genindex

.. comment:
   Indices and tables
   -----------------------
   * :ref:`genindex`
   * :ref:`search`
