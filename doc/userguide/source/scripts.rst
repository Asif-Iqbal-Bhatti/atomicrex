.. _utility_scripts:
.. index:: Utility scripts
   single: User defined structures; format conversion
   single: Structures; format conversion

Supporting scripts
******************************

Generating input files for force matching
------------------------------------------------

The `prepare_configurations_for_force_matching.py` script that can be
found in the `utility_scripts` directory allows one to create
structure files that contain not only positions but also forces
suitable for force matching. The script reads an `atomic simulation
environment (ASE) <https://wiki.fysik.dtu.dk/ase>`_ compatible input
file containing one or more configurations of positions and forces,
e.g. a `VASP OUTCAR file
<http://cms.mpi.univie.ac.at/vasp/vasp/vasp.html>`_ or a `.traj file
<https://wiki.fysik.dtu.dk/ase/ase/io.html#module-ase.io>`_. It then
writes one or more corresponding files in a modified `POSCAR format
<http://cms.mpi.univie.ac.at/vasp/vasp/POSCAR_file.html>`_ that
includes forces, which is suitable for force matching using
:program:`atomicrex`.

.. container:: toggle

   .. container:: header

      ..

   .. literalinclude:: ../../../utility_scripts/prepare_configurations_for_force_matching.py
      :linenos:
      :language: python


Generating XML code for the <structures> block
------------------------------------------------

The `prepare_structure_xml_file.py` script that can be found in the
`utility_scripts` directory allows one to generate XML code suitable
for insertion into the ``<structures>`` block of an :program:`atomicrex`
input file. The script reads a series of `atomic simulation environment
(ASE) <https://wiki.fysik.dtu.dk/ase>`_ compatible input files
containing one or more configurations of positions and forces.

.. container:: toggle

   .. container:: header

      ..

   .. literalinclude:: ../../../utility_scripts/prepare_structure_xml_file.py
      :linenos:
      :language: python
