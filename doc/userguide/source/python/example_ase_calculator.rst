.. highlight:: python
   :linenothreshold: 5

.. _example_python_ase_calculator:

.. index::
   single: Python interface; example
   single: Examples; Python interface


Example: Ase calculator
==============================================

This example illustrates the usage of atomicrex as a calculator in
the `Atomic Simulation Environment (ASE) 
<https://wiki.fysik.dtu.dk/ase/index.html>`_. A bcc-Fe structure is 
created and the forces, stress and total energy are calculated using 
an ABOP [MulErhAlb07a]_

Location
------------------

`examples/python_ase_calculator`

Input files
------------------

* `calculate_energy.py`: main input file

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_ase_calculator/calculate_energy.py
       :linenos:
       :language: python

* `main.xml`: file with definition of potential

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_ase_calculator/main.xml
       :linenos:
       :language: xml

* `Fe.tersoff`: definition of potential parameters in `Lammps/Tersoff format
  <http://lammps.sandia.gov/doc/pair_tersoff.html>`_

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_ase_calculator/Fe.tersoff
       :linenos:
       :language: text


		  
Output (files)
------------------

* `log.calculate_energy`: stdout from run calculating the energy

  .. container:: toggle

    .. container:: header

       ..

    .. literalinclude:: ../../../../examples/python_ase_calculator/reference_output/log.calculate_energy
       :linenos:
       :language: text
