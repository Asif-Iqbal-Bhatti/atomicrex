# Base image
FROM ubuntu:latest

# - upgrade the base packages
# - packages needed for building the code (cmake, compilers, core libraries)
# - packages needed for additional functionality (xml-parsing, optimization, math parser)
# - packages needed for building the homepage (wget, unzip)
RUN \
    apt-get update -qy \
    && \
    apt-get upgrade -qy \
    && \
    apt-get install -qy cmake \
                        build-essential \
                        gfortran \
                        libboost-dev \
                        libboost-filesystem-dev \
                        xxd \
    && \
    apt-get install -qy libxml2-dev \
                        libnlopt-dev \
                        libmuparser-dev \
    && \
    apt-get install -qy wget \
                        unzip \
    && \
    apt-get install -qy python3-pip

# - install Python packages via pip
RUN python3 -m pip install --upgrade pip \
    && \
    python3 -m pip install \
                   ase \
                   numpy \
                   pyyaml \
                   scipy \
                   sphinx \
                   sphinx-rtd-theme \
                   sphinx_sitemap

