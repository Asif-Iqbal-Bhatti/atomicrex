res
set term svg font 'Helvetica, 22'
set out 'objective_function.svg'
set enc iso_8859_1

#set size 1.1, 0.9

#--------------------------------------------------------
col_red    = '#D7191C'
col_orange = '#FDAE61'
col_yellow = '#FFFFBF'
col_blue1  = '#ABD9E9'
col_blue2  = '#2C7BB6'
col1 = col_blue2
col2 = col_orange
#------------
set style incr user
LW=3 ; PS = 1.8
LS=0
LS=LS+1 ; set style line LS lt 1 lw LW lc rgb col1
LS=LS+1 ; set style line LS lt 1 lw LW lc rgb col2
LS=LS+1 ; set style line LS lt 2 lw LW lc 0
#------------
set style incr user
LW=4
set style arr 11 lt 1 lw LW lc 0 filled head
set style arr 12 lt 1 lw LW lc 0 filled heads
set style arr 13 lt 1 lw 4 lc rgb '#bbbbbb' nohead
set style arr 14 lt 2 lw 4 lc 0 nohead

#--------------------------------------------------------
xc = 1e-3
set xla '{/=24 Trial step ({\327}10^3)}'
set log x
set yla '{/=24 Objective function ({\327}10^3)}' tc rgb col1
set yti nomi 5
set y2la '{/=24 Temperature}' tc rgb col2
set y2ti 20
#---
set key noautoti Left rev spac 1.4
set key right bot
p '<grep ^mc log.simulated-annealing' u (xc*$2):($4/1e3) ev 10 w l, \
  '<grep ^mc log.simulated-annealing' u (xc*$2):3 ev 10 axis x1y2 w l
