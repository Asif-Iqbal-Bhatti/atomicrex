# creates: pairpotential.svg

import matplotlib.pyplot as plt
import numpy as np

x_ref,y_ref = np.loadtxt('pairpotential_reference.table',
                         usecols=(1,2), unpack=True)
x_fit,y_fit = np.loadtxt('Cu_potential.V.table',
                         usecols=(1,2), unpack=True)

plt.figure(figsize=(4, 3))
plt.xlabel(r'Pair distance (A)')
plt.ylabel(r'Pair potential (eV)')
plt.axis([1.8, 5.5, -0.5, 2])
plt.plot(x_ref, y_ref,
         color='cornflowerblue',
         linestyle='-',
         label='from EAM potential')
plt.plot(x_fit, y_fit,
         color='orange',
         linestyle='-',
         label='fitted potential')
plt.legend()
plt.savefig('pairpotential.svg', bbox_inches='tight')
