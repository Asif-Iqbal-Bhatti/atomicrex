###############################################################################
#
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# The automated test suite currently requires a working Python interpreter.
# Note: Building the Atomicrex Python module must be enabled to find the Python interpreter.
IF(NOT PYTHON_EXECUTABLE)
	RETURN()
ENDIF()

# Enable CMake's testing framework.
ENABLE_TESTING()

# Generate a test case from every .py file under the tests/ source directory.
FILE(GLOB_RECURSE PYTHON_TEST_SCRIPTS "${CMAKE_SOURCE_DIR}/tests/*.py")
FOREACH(PYTHON_TEST_SCRIPT ${PYTHON_TEST_SCRIPTS})

	# Execute the test script using our current Python interpreter.
	# The working directory is set to directory containing the script.
	GET_FILENAME_COMPONENT(_WORKING_DIR "${PYTHON_TEST_SCRIPT}" DIRECTORY)
	ADD_TEST(NAME "${PYTHON_TEST_SCRIPT}"
			 WORKING_DIRECTORY "${_WORKING_DIR}"
			 COMMAND "${PYTHON_EXECUTABLE}" "${PYTHON_TEST_SCRIPT}")

	# Need to pass path of the atomicrex module to the Python interpreter via PYTHONPATH env variable.
	SET_PROPERTY(TEST "${PYTHON_TEST_SCRIPT}" PROPERTY ENVIRONMENT "PYTHONPATH=${CMAKE_BINARY_DIR}/python:$ENV{PYTHONPATH};ATOMICREX_EXECUTABLE=${CMAKE_BINARY_DIR}/atomicrex")

ENDFOREACH()

# Honor visibility properties for all target types.
# This is needed so we can set the symbol visibility of the compile targets to hidden below. 
CMAKE_POLICY(SET CMP0063 NEW)

# Generate a test case from every .py file under the tests/ source directory.
FILE(GLOB_RECURSE UNIT_TESTS "${CMAKE_SOURCE_DIR}/tests/*.cpp")
FOREACH(UNIT_TEST ${UNIT_TESTS})
    # create a unique name of the executable from the filename and get working directory
	get_filename_component(testname ${UNIT_TEST} NAME)
	get_filename_component(working_dir ${UNIT_TEST} DIRECTORY)
	# creates the executable
	add_executable("${testname}" "${UNIT_TEST}")
	# indicates the include paths
	target_include_directories("${testname}" PRIVATE ${BOOST_INCLUDE_DIRS})
	target_link_libraries("${testname}" PRIVATE atomicrexlib)
	# Compile with symbols hidden by default. This is needed because the atomicrex lib is compiled in the same way.
	SET_TARGET_PROPERTIES("${testname}" PROPERTIES CXX_VISIBILITY_PRESET "hidden")
	add_test(NAME "${UNIT_TEST}" 
			 WORKING_DIRECTORY "${working_dir}"
			 COMMAND "${testname}")
ENDFOREACH()
