###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Try to find the Nlopt library
#  NLOPT_FOUND - system has nlopt
#  NLOPT_INCLUDE_DIRS - the include directories needed
#  NLOPT_LIBRARIES - Link libraries needed to use nlopt

FIND_PATH(NLOPT_INCLUDE_DIR NAMES nlopt.hpp)
FIND_LIBRARY(NLOPT_LIBRARY NAMES nlopt)

SET(NLOPT_INCLUDE_DIRS ${NLOPT_INCLUDE_DIR})
SET(NLOPT_LIBRARIES ${NLOPT_LIBRARY})

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NLOPT DEFAULT_MSG NLOPT_LIBRARY NLOPT_INCLUDE_DIR)

MARK_AS_ADVANCED(NLOPT_INCLUDE_DIR NLOPT_LIBRARY)
