###############################################################################
# 
#  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
#
#  This file is part of atomicrex.
#
#  Atomicrex is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Atomicrex is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Current code release version is defined by PROJECT() command in root CMake file.
SET(ATOMICREX_VERSION_STRING "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")

# Extract revision tag name from Git repository and append it to the version string.
FIND_PACKAGE(Git)
IF(GIT_FOUND)
	EXECUTE_PROCESS(COMMAND ${GIT_EXECUTABLE} "describe" "--always" WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
					RESULT_VARIABLE GIT_RESULT_VAR 
					OUTPUT_VARIABLE GIT_TAG_STRING
					OUTPUT_STRIP_TRAILING_WHITESPACE)
	IF(NOT GIT_RESULT_VAR STREQUAL "0")
		MESSAGE(FATAL "Failed to execute Git: ${GIT_RESULT_VAR}")
	ENDIF()
	
	SET(ATOMICREX_VERSION_STRING "${ATOMICREX_VERSION_STRING}-${GIT_TAG_STRING}")
ENDIF()
