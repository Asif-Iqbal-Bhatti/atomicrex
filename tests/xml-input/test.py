from __future__ import print_function, division
from subprocess import call
from glob import glob
from os import environ

for fname in glob('main*.xml'):
    print('\n\nfilename: {}\n'.format(fname))
    return_code = call([environ['ATOMICREX_EXECUTABLE'], fname])
    assert return_code == 0
    print('')
