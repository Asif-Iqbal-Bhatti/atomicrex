from __future__ import print_function
from subprocess import Popen, PIPE
from glob import glob
import yaml
import os


def get_atomic_energies(f):
    energies = {}
    for line in f.readlines():
        # this takes care of Python3 compatibility
        line = line.decode('UTF-8')
        if 'Structure' in line:
            structure = line.split()[1]
            structure = structure.replace("'", '')
            structure = structure.replace(':', '')
        elif 'atomic-energy' in line:
            energies[structure] = float(line.split()[1])
    return energies

if __name__ == '__main__':

    with open('reference-data.yml', 'r') as infile:
        refdata = yaml.load(infile, Loader=yaml.FullLoader)
    for fname in glob('main*.xml'):
        print('filename: {}'.format(fname))
        proc = Popen([os.environ['ATOMICREX_EXECUTABLE'], fname], stdout=PIPE)
        energies = get_atomic_energies(proc.stdout)
        data = energies
        for key in data:
            assert abs(data[key] - refdata[fname][key]) < 1e-7

    for fname in ['Cu.eam.fs', 'Cu.pairpot.V.table', 'lj.xml']:
        try:
            os.remove(fname)
        except OSError:
            pass
