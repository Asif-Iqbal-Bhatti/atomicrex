import atomicrex
import os

# Test if atomic energy and lattice constant match target values.
# The parameter constraints applied should guarante an exact fit
# Also check bulk module which is not explicitly fitted, but supplied
# within the parameter constraints

job = atomicrex.Job()
job.parse_input_file('main.xml')
job.prepare_fitting()
job.perform_fitting()

fcc = job.structures["FCC_Cu"]
energy = fcc.get_potential_energy()
e_diff = energy - -3.5399116947014
assert e_diff < 1e-12 
a = fcc.properties["lattice-parameter"].computed_value
a_diff = a -3.61
assert a_diff < 1e-12
bm = fcc.properties["bulk-modulus"].computed_value
bm_diff = abs(bm-140.0) # bm value put in via parameter constraint
assert bm_diff < 1.0
